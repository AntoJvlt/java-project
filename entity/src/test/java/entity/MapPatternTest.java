/*
 * @author Jolivat Antonin - Favier Paulin - Doittée Anthime - Zaafane Rania
 */
package entity;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * The Class MapPatternTest.
 */
public class MapPatternTest
{

	/**
	 * Test map pattern.
	 */
	@Test
	public void testMapPattern()
	{
		MapPattern mapPattern = new MapPattern("xxx");
		assertEquals(mapPattern.getMap(), "xxx");
	}

}
