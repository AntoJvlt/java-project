/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package entity;

/**
 * The Class MapPattern.
 * 
 * The MapPattern entity extracted from the database is used by the map.
 */
public class MapPattern extends Entity {
	
	/** The map. */
	private String map;

	/**
	 * Gets the map pattern.
	 *
	 * @return the map pattern
	 */
	public String getMap() {
		return map;
	}

	/**
	 * Sets the map pattern.
	 *
	 * @param map the new map pattern
	 */
	public void setMap(String map) {
		this.map = map;
	}
	
	/**
	 * Instantiates a new map pattern.
	 *
	 * @param map the map pattern
	 */
	public MapPattern(String map)
	{
		setMap(map);
	}
}
