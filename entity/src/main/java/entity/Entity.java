/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package entity;

/**
 * The Class Entity.
 * 
 * For future implementations which could need it
 */
public abstract class Entity  {
}
