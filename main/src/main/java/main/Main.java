/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package main;

import java.io.IOException;

import controller.GameController;
import model.GameModel;
import view.View;

/**
 * The Class Main.
 */
public abstract class Main {
	    
    	/**
    	 * The main method.
    	 *
    	 * @param args the arguments
    	 * @throws IOException Signals that an I/O exception has occurred.
    	 */
    public static void main(final String[] args) throws IOException {
        final GameModel gameModel = new GameModel();
        final View view = new View(gameModel);
        final GameController gameController = new GameController(gameModel, view);
        gameController.startGame();
		}
    }
