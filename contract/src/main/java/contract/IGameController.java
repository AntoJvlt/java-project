/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package contract;

/**
 * The Interface IGameController
 * 
 * Implemented by the GameController
 * Contains all the game's parameter constants.
 */
public interface IGameController
{
	/** The numero the map that will be loaded from the data base**/
	int NUMERO_MAP = 3;
	
	/** The quotient determine how much elements are displayed on the screen **/
	/** Can't be under 1 **/
	double QUOTIENT_DISPLAYED_ELEMENTS = 1.5;

	/** The timer duration before the player loose  **/
	/** In seconds **/
	int TIMER_DURATION = 300;

	/** The speed of the player **/
	/** Between 0 and 100**/
	static int PLAYER_MOVEMENT_SPEED = 100;

	/** The collapsing speed determine how fast the gravitables elements collapse **/
	/** Between 0 and 1000 **/
	static int COLLAPSING_SPEED = 800;
	
	/** The monster speed determine how fast the monsters move **/
	/** Between 0 and 1000 **/
	static int MONSTER_SPEED = 800;
	
	/**
	 * Gets the remaining time.
	 *
	 * @return the remaining time
	 */
	int getRemainingTime();
	
	/**
	 * Gets the current score.
	 *
	 * @return the current score
	 */
	int getCurrentScore();
	
	/**
	 * Gets the game model.
	 *
	 * @return the game model.
	 */
	IGameModel getGameModel();
}
