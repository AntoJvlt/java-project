/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package contract;

/**
 * The Interface IView.
 * 
 * Implemented by the view.
 */
public interface IView
{
	
	/**
	 * Update the camera.
	 *
	 * @param direction the direction
	 */
	void updateCamera(Direction direction);
	
	/**
	 * Sets the controller.
	 *
	 * @param playerMouvementController the new controller
	 */
	void setController(IPlayerMouvementController playerMouvementController);
	
	/**
	 * Show victory message.
	 *
	 * @param message the message
	 */
	void showMessage(String message);
	
	/**
	 * Show reloading message.
	 */
	void showReloadingMessage();
	
	/**
	 * Center camera.
	 */
	void centerCamera();
	
	/**
	 * Reboot camera.
	 */
	void rebootCamera();
}
