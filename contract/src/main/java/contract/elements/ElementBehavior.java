/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package contract.elements;

/**
 * The Enum ElementBehavior.
 * 
 * Behavior of the elements.
 */
public enum ElementBehavior
{
	/** Can't be crossed. */
	BLOCKING,

	/** Can be crossed. */
	PERMEABLE,
}
