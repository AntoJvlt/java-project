/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package contract.elements;

/**
 * The Interface IKilleable.
 * 
 * Implemented by elements which can be killed.
 */
public interface IKilleable
{
	/**
	 * kill the element.
	 *
	 * @param deathMessage the death message
	 */
	void die(String deathMessage);
}
