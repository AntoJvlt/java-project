/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package contract.elements;

/**
 * The Interface ICollectable.
 * 
 * Implemented by elements which can be collected by the player.
 */
public interface ICollectable
{
	/**
	 * Collect the element.
	 */
	void collect();
}
