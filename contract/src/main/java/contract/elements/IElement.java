/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package contract.elements;

import contract.IMap;

/**
 * The Interface IElement.
 * 
 * Implemented by the elements.
 */
public interface IElement
{
	/**
	 * Gets the element's behavior.
	 *
	 * @return the behavior
	 */
	ElementBehavior getBehavior();

	/**
	 * Execute the collision action of the element.
	 */
	void execCollisionAction();
	
	/**
	 * Get the map.
	 * @return the map.
	 */
	IMap getMap();
}
