/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package contract.elements;

/**
 * The Interface IDiggable.
 * 
 * Implemented by elements which can be dig.
 */
public interface IDiggable
{
	/**
	 * Dig the element.
	 */
	void dig();
}
