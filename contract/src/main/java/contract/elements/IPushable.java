/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package contract.elements;

import contract.IMap;

/**
 * The Interface IPushable.
 * 
 * Implemented by elements which can be pushed.
 */
public interface IPushable
{
	/**
	 * Push the element on the right.
	 *
	 * @param map the map
	 */
	void pushRight(IMap map);

	/**
	 * Push the element on the left.
	 *
	 * @param map the map
	 */
	void pushLeft(IMap map);

	/**
	 * Check if the element can be pushed on the right.
	 *
	 * @return true, if he can be pushed
	 */
	boolean canBePushedRight();

	/**
	 * sCheck if the element can be pushed on the left.
	 *
	 * @return true, if he can be pushed
	 */
	boolean canBePushedLeft();

}
