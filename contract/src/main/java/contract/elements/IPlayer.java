/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package contract.elements;

/**
 * The Interface IPlayer.
 * 
 * Implemented by the player element.
 */
public interface IPlayer
{
	/**
	 * Gets the pos X.
	 *
	 * @return the pos X
	 */
	int getPosX();

	/**
	 * Gets the pos Y.
	 *
	 * @return the pos Y
	 */
	int getPosY();

	/**
	 * Move up.
	 */
	void moveUp();

	/**
	 * Move right.
	 */
	void moveRight();

	/**
	 * Move down.
	 */
	void moveDown();

	/**
	 * Move left.
	 */
	void moveLeft();

	/**
	 * Start the graphics animations.
	 */
	void startAnimation();

	/**
	 * Stop the graphics animations.
	 */
	void stopAnimation();

	/**
	 * Checks if the player is alive.
	 *
	 * @return true, if the player is alive
	 */
	boolean isAlive();

	/**
	 * Gets the death message.
	 *
	 * @return the death message
	 */
	String getDeathMessage();
}
