/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package contract.elements;

/**
 * The Interface IKiller.
 * 
 * Implemented by elements which are killer.
 */
public interface IKiller
{
	/**
	 * Kill an element.
	 */
	void kill();
}
