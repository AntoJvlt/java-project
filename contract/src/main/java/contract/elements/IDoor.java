/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package contract.elements;

/**
 * The Interface IDoor.
 * 
 * Implemented by door elements.
 */
public interface IDoor
{
	/**
	 * Checks if the door is open.
	 *
	 * @return true, if it's open
	 */
	boolean isOpen();

	/**
	 * Open the door.
	 */
	void open();
}
