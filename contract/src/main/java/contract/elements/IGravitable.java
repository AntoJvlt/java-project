/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package contract.elements;

import contract.IMap;

/**
 * The Interface IGravitable.
 * 
 * Implemented by elements which are under gravity.
 */
public interface IGravitable extends ITriggering
{
	/**
	 * Checks if the element is falling.
	 *
	 * @return true, if it's falling
	 */
	boolean isFalling();

	/**
	 * Make the element fall.
	 *
	 * @param map the map
	 */
	void fall(IMap map);
}
