/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package contract.elements;

import java.util.ArrayList;

import contract.IMap;
import contract.IPawn;

/**
 * The Interface ITriggering.
 * 
 * Implemented by elements which can trigger others elements to make them fall.
 */
public interface ITriggering
{
	/**
	 * Gets all the elements which will be trigger.
	 *
	 * @param elem the element who want to trigger
	 * @param map the map
	 * @return the i pawns to trigger
	 */
	public static ArrayList<IPawn> getIPawnsToTrigger(IPawn elem, IMap map)
	{
		ArrayList<IPawn> ipawnToTrigger = new ArrayList<>();

		/* Add top IPawn */
		if (map.searchIpawnAtPos(elem.getX(), elem.getY() - 1) != null)
			ipawnToTrigger.add(map.searchIpawnAtPos(elem.getX(), elem.getY() - 1));

		/* Add right IPawn */
		if (map.searchIpawnAtPos(elem.getX() + 1, elem.getY()) != null)
			ipawnToTrigger.add(map.searchIpawnAtPos(elem.getX() + 1, elem.getY()));

		/* Add top right IPawn */
		if (map.searchIpawnAtPos(elem.getX() + 1, elem.getY() - 1) != null)
			ipawnToTrigger.add(map.searchIpawnAtPos(elem.getX() + 1, elem.getY() - 1));

		/* Add left IPawn */
		if (map.searchIpawnAtPos(elem.getX() - 1, elem.getY()) != null)
			ipawnToTrigger.add(map.searchIpawnAtPos(elem.getX() - 1, elem.getY()));

		/* Add top left IPawn */
		if (map.searchIpawnAtPos(elem.getX() - 1, elem.getY() - 1) != null)
			ipawnToTrigger.add(map.searchIpawnAtPos(elem.getX() - 1, elem.getY() - 1));

		return ipawnToTrigger;
	}

	/**
	 * Start trigger around.
	 *
	 * @param elemsToTrigger the elements to trigger
	 * @param map the map
	 */
	void startTriggerAround(ArrayList<IPawn> elemsToTrigger, IMap map);
}
