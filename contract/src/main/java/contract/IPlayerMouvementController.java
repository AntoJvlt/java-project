/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package contract;

/**
 * The Interface IPlayerMouvementController.
 */
public interface IPlayerMouvementController{

	/**
	 * Control.
	 */
	public void control();

	/**
	 * Order perform.
	 *
	 * @param controllerOrder the controller order
	 */
	public void orderPerform(Direction controllerOrder);
	
	/**
	 * Get the gameController.
	 * 
	 * @return the game controller
	 */
	IGameController getGameController();
	
}
