/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package contract;

import java.util.ArrayList;

import contract.elements.IDoor;


/**
 * The Interface IMap.
 * 
 * Implemented by the map.
 */
public interface IMap
{
	/**
	 * Gets the map width.
	 *
	 * @return the map width
	 */
	int getMapWidth();

	/**
	 * Gets the map height.
	 *
	 * @return the map height
	 */
	int getMapHeight();

	/**
	 * Gets the element on the map.
	 *
	 * @param x the x
	 * @param y the y
	 * @return the element on the map
	 */
	ISquare getElementOnTheMap(int x, int y);

	/**
	 * Gets the mobiles elements.
	 *
	 * @return the mobiles elements
	 */
	ArrayList<IPawn> getMobilesElements();
	
	/**
	 * Search ipawn at pos.
	 *
	 * @param x the x
	 * @param y the y
	 * @return the i pawn
	 */
	IPawn searchIpawnAtPos (int x,int y);
	
	/**
	 * Destroy ipawn.
	 *
	 * @param ipawn the ipawn
	 */
	void destroyIpawn(IPawn ipawn);
	
	/**
	 * Gets the game model.
	 *
	 * @return the game model
	 */
	IGameModel getGameModel();
	
	/**
	 * Gets the door.
	 *
	 * @return the door
	 */
	IDoor getDoor();
	
	/**
	 * Gets the tab map.
	 *
	 * @return the tab map
	 */
	ISquare[][] getTabMap();
	
	/**
	 * Check if the map is modified.
	 *
	 * @return true if the map is being modified
	 */
	boolean isBeingModified();
}
