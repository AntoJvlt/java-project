/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package contract;

/**
 * The Enum Direction.
 * 
 * The directions.
 */
public enum Direction
{
	/** Upward direction */
	UP,
	
	/** Right direction. */
	RIGHT,
	
	/** Down direction. */
	DOWN,
	
	/** Left direction. */
	LEFT
}
