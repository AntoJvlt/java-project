/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package contract;

import java.util.Observable;

import contract.elements.IPlayer;

/**
 * The Interface IGameModel.
 * 
 * Implemented by the GameModel which is the core of the model.
 */
public interface IGameModel
{
	/**
	 * Gets the map.
	 *
	 * @return the map
	 */
	IMap getMap();

	/**
	 * Gets the observable.
	 *
	 * @return the observable
	 */
	Observable getObservable();

	/**
	 * Notify the observers.
	 */
	void updateChanges();

	/**
	 * Gets the player.
	 *
	 * @return the player
	 */
	IPlayer getPlayer();

	/**
	 * Gets the number of diamond collected by the player.
	 *
	 * @return the number of diamond collected
	 */
	int getNumberDiamondCollected();

	/**
	 * Gets the number of total diamond on the map.
	 *
	 * @return the total number of diamonds
	 */
	int getNumberDiamondTotal();
	
	/**
	 * 
	 * Reset the game.
	 * All the data are reset.
	 *
	 * @return true, if successful
	 */
	boolean resetGame();

	/**
	 * Check if the player has won.
	 *
	 * @return true, if the player has won
	 */
	boolean playerWon();

	/**
	 * Register that the player won.
	 */
	void setPlayerWon();
	
	/**
	 * Check if the game is reloading.
	 * @return true if the game is reloading
	 */
	boolean gameIsReloading();
}
