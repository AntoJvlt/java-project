/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package controller;

import contract.Direction;
import contract.IPlayerMouvementController;
import contract.elements.ElementBehavior;
import contract.elements.ICollectable;
import contract.elements.IDiggable;
import contract.elements.IElement;
import contract.elements.IGravitable;
import contract.elements.IKiller;
import contract.elements.IPushable;

/**
 * The Class PlayerMouvementController.
 * 
 * This class is the controller for the player's movements.
 * It control the movement when the user press a key to move.
 */
public class PlayerMouvementController implements IPlayerMouvementController
{

	/** The direction order. */
	private Direction directionOrder;

	/** The next I square. */
	private IElement nextISquare;

	/** The next I pawn. */
	private IElement nextIPawn;

	/** The movement delayer. */
	private MovementDelayer movementDelayer;

	/** The game controller. */
	private GameController gameController;

	/**
	 * Instantiates a new player mouvement controller.
	 *
	 * @param gameController the game controller
	 */
	public PlayerMouvementController(GameController gameController)
	{
		this.gameController = gameController;
		this.movementDelayer = new MovementDelayer();
	}

	/* (non-Javadoc)
	 * @see contract.IPlayerMouvementController#orderPerform(contract.Direction)
	 */
	@Override
	public void orderPerform(Direction directionOrder)
	{
		this.setDirectionOrder(directionOrder);

		if (this.getGameController().gameIsRunning() && this.getDirectionOrder() != null && movementDelayer.canMoove())
		{
			int currentPosX = this.getGameController().getGameModel().getPlayer().getPosX();
			int currentPosY = this.getGameController().getGameModel().getPlayer().getPosY();

			switch (this.getDirectionOrder())
			{
			case UP:
				this.nextISquare = (IElement) this.getGameController().getGameModel().getMap().getElementOnTheMap(currentPosX, currentPosY - 1);
				this.nextIPawn = (IElement) this.getGameController().getGameModel().getMap().searchIpawnAtPos(currentPosX, currentPosY - 1);
				break;
			case RIGHT:
				this.nextISquare = (IElement) this.getGameController().getGameModel().getMap().getElementOnTheMap(currentPosX + 1, currentPosY);
				this.nextIPawn = (IElement) this.getGameController().getGameModel().getMap().searchIpawnAtPos(currentPosX + 1, currentPosY);
				break;
			case DOWN:
				this.nextISquare = (IElement) this.getGameController().getGameModel().getMap().getElementOnTheMap(currentPosX, currentPosY + 1);
				this.nextIPawn = (IElement) this.getGameController().getGameModel().getMap().searchIpawnAtPos(currentPosX, currentPosY + 1);
				break;
			case LEFT:
				this.nextISquare = (IElement) this.getGameController().getGameModel().getMap().getElementOnTheMap(currentPosX - 1, currentPosY);
				this.nextIPawn = (IElement) this.getGameController().getGameModel().getMap().searchIpawnAtPos(currentPosX - 1, currentPosY);
				break;
			}
			this.control();
		}
	}

	/* (non-Javadoc)
	 * @see contract.IPlayerMouvementController#control()
	 */
	@Override
	public void control()
	{
		boolean allowMovement = false;

		if (nextISquare.getBehavior() == ElementBehavior.PERMEABLE)
		{
			if (nextIPawn != null)
			{
				if (nextIPawn instanceof IPushable && (this.getDirectionOrder() == Direction.RIGHT || this.getDirectionOrder() == Direction.LEFT)) // Can be pushed
				{
					if (this.getDirectionOrder() == Direction.RIGHT && ((IPushable) nextIPawn).canBePushedRight()) // Check if the element can be pushed
																													// on the right
					{
						((IPushable) nextIPawn).pushRight(this.getGameController().getGameModel().getMap());
						allowMovement = true;
					}
					else if (this.getDirectionOrder() == Direction.LEFT && ((IPushable) nextIPawn).canBePushedLeft()) // Check if the element can be pushed
																														// on the left
					{
						((IPushable) nextIPawn).pushLeft(this.getGameController().getGameModel().getMap());
						allowMovement = true;
					}
				}
				if (nextIPawn instanceof ICollectable) // Check if the element is collectable
				{
					if (nextIPawn instanceof IGravitable) // Check if he can be falling
					{
						if (!(((IGravitable) nextIPawn).isFalling())) // Check if he is not falling
							allowMovement = true;
					}
					else // If he is not a gravitable element, he can't be falling
					{
						allowMovement = true;
					}
				}
				if (nextIPawn instanceof IKiller)
					allowMovement = true;

			}
			else
				allowMovement = true;
		}
		else
		{
			if (nextISquare instanceof IDiggable) // If the element is diggable the movement is allowed
				allowMovement = true;
		}

		if (allowMovement)
		{
			this.movePlayer();
			this.getGameController().getView().updateCamera(this.getDirectionOrder());
		}
	}

	/**
	 * Move player.
	 */
	private void movePlayer()
	{
		if (this.nextIPawn != null)
			nextIPawn.execCollisionAction();
		else
			nextISquare.execCollisionAction();

		switch (this.directionOrder)
		{
		case UP:
			this.getGameController().getGameModel().getPlayer().moveUp();
			break;
		case RIGHT:
			this.getGameController().getGameModel().getPlayer().moveRight();
			break;
		case DOWN:
			this.getGameController().getGameModel().getPlayer().moveDown();
			break;
		case LEFT:
			this.getGameController().getGameModel().getPlayer().moveLeft();
			break;
		}

		this.movementDelayer.delayMovement();
	}

	/**
	 * Sets the direction order.
	 *
	 * @param directionOrder the new direction order
	 */
	private void setDirectionOrder(Direction directionOrder)
	{
		this.directionOrder = directionOrder;
	}

	/**
	 * Gets the direction order.
	 *
	 * @return the direction order
	 */
	private Direction getDirectionOrder()
	{
		return this.directionOrder;
	}

	/* (non-Javadoc)
	 * @see contract.IPlayerMouvementController#getGameController()
	 */
	@Override
	public GameController getGameController()
	{
		return this.gameController;
	}

}
