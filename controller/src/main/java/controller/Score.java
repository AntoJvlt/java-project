/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package controller;

/**
 * The Class Score.
 * 
 * This class is used to calculate and return the score.
 */
public class Score {

	/** The score. */
	private int score;

	/** The game controller. */
	GameController gameController;
	
	/**
	 * Instantiates a new score.
	 *
	 * @param gameController the game controller
	 */
	public Score(GameController gameController)
	{
		this.gameController = gameController;
	}
	
	/**
	 * Gets the score.
	 *
	 * @return the score
	 */
	public int getScore() {
		return this.score;
	}

	/**
	 * Update score.
	 */
	public void updateScore() {
		this.score = this.getGameController().getTimer().getTime() * 10 + this.getGameController().getGameModel().getNumberDiamondCollected() * 100;
	}
	
	/**
	 * Gets the game controller.
	 *
	 * @return the game controller
	 */
	private GameController getGameController()
	{
		return this.gameController;
	}
}
