/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package controller;

import contract.IGameController;
/**
 * The Class Timer.
 */
public class Timer implements Runnable
{

	/** The game controller. */
	private GameController gameController;

	/** The timer thread. */
	private Thread timerThread;

	/** The Constant MAX_TIME. */
	public final static int MAX_TIME = IGameController.TIMER_DURATION;

	/** The current time. */
	private int currentTime;

	/**
	 * Instantiates a new timer.
	 *
	 * @param gameController the game controller
	 */
	public Timer(GameController gameController)
	{
		this.gameController = gameController;
	}

	/**
	 * Start.
	 */
	@SuppressWarnings("deprecation")
	public void start()
	{
		this.currentTime = this.MAX_TIME;

		this.stop();

		this.timerThread = new Thread(this);
		this.timerThread.start();
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run()
	{
		for (int i = this.MAX_TIME; i > 0; i--)
		{
			if (this.gameController.gameIsRunning())
			{
				this.currentTime--;

				if (!this.gameController.getGameModel().gameIsReloading())
					this.getGameController().getGameModel().updateChanges();

				try
				{
					Thread.sleep(1000);

				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		}

		this.getGameController().endGame("Le temps est écoulé..." + this.getGameController().getStatsMessage());
	}

	/**
	 * Gets the game controller.
	 *
	 * @return the game controller
	 */
	private GameController getGameController()
	{
		return this.gameController;
	}

	/**
	 * Stop.
	 */
	public void stop()
	{
		if (this.timerThread != null)
			this.timerThread.stop();
	}

	/**
	 * Gets the time.
	 *
	 * @return the time
	 */
	public int getTime()
	{
		return this.currentTime;
	}
}
