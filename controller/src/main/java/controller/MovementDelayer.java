/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package controller;

import contract.IGameController;

/**
 * The Class MovementDelayer.
 * 
 * This class is used as a timer to slow down the player's movement capacity
 */
public class MovementDelayer implements Runnable
{

	/** The can moove. */
	private boolean canMoove = true;

	/** The Constant TIME_DELAY. */
	private final static int TIME_DELAY = 100 - IGameController.PLAYER_MOVEMENT_SPEED;

	/** The thread. */
	private Thread thread;

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run()
	{
		try
		{
			Thread.sleep(this.TIME_DELAY);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}

		if (!canMoove)
			this.canMoove = true;
	}

	/**
	 * Can moove.
	 *
	 * @return true, if successful
	 */
	public boolean canMoove()
	{
		return this.canMoove;
	}

	/**
	 * Delay movement.
	 */
	public void delayMovement()
	{
		this.canMoove = false;
		this.thread = new Thread(this);
		this.thread.start();
	}
}
