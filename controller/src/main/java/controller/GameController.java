/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package controller;

import contract.IGameController;
import contract.IGameModel;
import contract.IView;

/**
 * The Class GameController.
 * 
 * Control the game, here we can restart the game, get the stats, and display message.
 */
public class GameController implements Runnable, IGameController
{

	/** The view. */
	private IView view;

	/** The game model. */
	private IGameModel gameModel;

	/** The game thread. */
	private Thread gameThread;

	/** The game is running. */
	private boolean gameIsRunning = false;

	/** The timer. */
	private Timer timer = new Timer(this);

	/** The score. */
	private Score score = new Score(this);

	/**
	 * Instantiates a new game controller.
	 *
	 * @param gameModel the game model
	 * @param view the view
	 */
	public GameController(IGameModel gameModel, IView view)
	{
		this.gameModel = gameModel;
		this.view = view;

		this.getView().setController(new PlayerMouvementController(this));
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run()
	{
		while (true)
		{
			try
			{
				Thread.sleep(50);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}

			this.getScore().updateScore();

			if (this.getGameModel().playerWon())
				this.endGame("<html><body>You won !" + this.getStatsMessage());

			if (!this.getGameModel().getPlayer().isAlive())
				this.endGame("<html><body>You died " + this.getGameModel().getPlayer().getDeathMessage() + this.getStatsMessage());
		}
	}

	/**
	 * Gets the stats message.
	 *
	 * @return the stats message
	 */
	public String getStatsMessage()
	{
		return "<br><br>Remaining time : " + this.getTimer().getTime() + " seconds <br><br> Score : " + this.getScore().getScore() + "</body></html>";
	}

	/**
	 * Start game.
	 */
	public void startGame()
	{
		this.setGameIsRunning(true);

		this.gameThread = new Thread(this);
		this.gameThread.start();
		this.getTimer().start();
	}

	/**
	 * End game.
	 *
	 * @param message the message
	 */
	public void endGame(String message)
	{
		this.setGameIsRunning(false);
		this.getTimer().stop();
		this.getView().showMessage(message);
		this.restartGame();
	}

	/**
	 * Restart game.
	 */
	public void restartGame()
	{
		this.setGameIsRunning(false);
		this.getTimer().stop();

		this.getView().centerCamera();

		this.getGameModel().updateChanges();

		this.getView().showReloadingMessage();

		if (this.getGameModel().resetGame())
		{
			this.getView().rebootCamera();
			this.setGameIsRunning(true);
			this.getTimer().start();
		}
	}

	/**
	 * Gets the view.
	 *
	 * @return the view
	 */
	public IView getView()
	{
		return this.view;
	}

	/* (non-Javadoc)
	 * @see contract.IGameController#getGameModel()
	 */
	@Override
	public IGameModel getGameModel()
	{
		return this.gameModel;
	}

	/**
	 * Gets the timer.
	 *
	 * @return the timer
	 */
	public Timer getTimer()
	{
		return this.timer;
	}

	/**
	 * Gets the score.
	 *
	 * @return the score
	 */
	public Score getScore()
	{
		return this.score;
	}

	/**
	 * Game is running.
	 *
	 * @return true, if successful
	 */
	public boolean gameIsRunning()
	{
		return this.gameIsRunning;
	}

	/**
	 * Sets the game is running.
	 *
	 * @param gameIsRunning the new game is running
	 */
	public void setGameIsRunning(boolean gameIsRunning)
	{
		this.gameIsRunning = gameIsRunning;
	}

	/* (non-Javadoc)
	 * @see contract.IGameController#getRemainingTime()
	 */
	@Override
	public int getRemainingTime()
	{
		return this.getTimer().getTime();
	}

	/* (non-Javadoc)
	 * @see contract.IGameController#getCurrentScore()
	 */
	@Override
	public int getCurrentScore()
	{
		return this.getScore().getScore();
	}
}
