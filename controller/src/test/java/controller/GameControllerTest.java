/*
 * @author Jolivat Antonin - Favier Paulin - Doittée Anthime - Zaafane Rania
 */
package controller;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import contract.Direction;
import contract.IGameModel;
import contract.IPlayerMouvementController;
import contract.IView;


/**
 * The Class GameControllerTest.
 */
public class GameControllerTest
{


	/**
	 * Test game controller.
	 *
	 * @param <GameModel> the generic type
	 */
	@Test
	public <GameModel> void testGameController()
	{
		GameModel gameModel = null;
		IView iView = new IView()
		{
			
			@Override
			public void updateCamera(Direction directionOrder)
			{
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void showReloadingMessage()
			{
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void setController(IPlayerMouvementController playerMouvementController)
			{
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void rebootCamera()
			{
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void centerCamera()
			{
				// TODO Auto-generated method stub
				
			}

			@Override
			public void showMessage(String message)
			{
				// TODO Auto-generated method stub
				
			}
		};
		GameController gameControler = new GameController((IGameModel) gameModel , iView);
		assertEquals(gameControler.getStatsMessage(),"<br><br>Remaining time : 0 seconds <br><br> Score : 0</body></html>");
		
	}

}
