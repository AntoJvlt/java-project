/*
 * @author Jolivat Antonin - Favier Paulin - Doittée Anthime - Zaafane Rania
 */
package view;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import model.GameModel;

import model.*;

/**
 * The Class ViewTest.
 */
public class ViewTest
{

	/**
	 * Test show reloading message.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testShowReloadingMessage() throws IOException
	{
		GameModel gameModel = new GameModel();
		View view = new View(gameModel);
		view.showDefeatMessage("Hello");

	}

}
