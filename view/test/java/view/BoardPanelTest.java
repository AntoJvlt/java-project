/*
 * @author Jolivat Antonin - Favier Paulin - Doittée Anthime - Zaafane Rania
 */
package view;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import model.GameModel;

import model.*;

/**
 * The Class BoardPanelTest.
 */
public class BoardPanelTest
{

	/**
	 * Test find component.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Test
	public void testFindComponent() throws IOException
	{
		GameModel gameModel = new GameModel();
		BoardPanel boardPanel = new BoardPanel(gameModel);
		assertNull(boardPanel.findComponentAt(4, 1));
	}

}
