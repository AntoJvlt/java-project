/*
 * @author Jolivat Antonin - Favier Paulin - Doittée Anthime - Zaafane Rania
 */
package view;

import static org.junit.Assert.*;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ElementVisitor;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.Name;
import javax.lang.model.type.TypeMirror;

import org.junit.Test;

import model.GameModel;
import model.Map;
import model.*;

/**
 * The Class GraphicsBuilderTest.
 */
public class GraphicsBuilderTest
{

	/**
	 * Test add elements to graphics.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws SQLException the SQL exception
	 */
	@Test
	public void testAddElementsToGraphics() throws IOException, SQLException
	{
		Element elem = new Element()
		{

			@Override
			public <A extends Annotation> A[] getAnnotationsByType(Class<A> annotationType)
			{
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Name getSimpleName()
			{
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Set<Modifier> getModifiers()
			{
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public ElementKind getKind()
			{
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Element getEnclosingElement()
			{
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<? extends Element> getEnclosedElements()
			{
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<? extends AnnotationMirror> getAnnotationMirrors()
			{
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public <A extends Annotation> A getAnnotation(Class<A> annotationType)
			{
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public TypeMirror asType()
			{
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public <R, P> R accept(ElementVisitor<R, P> v, P p)
			{
				// TODO Auto-generated method stub
				return null;
			}
		};
		GameModel gameModel = new GameModel();
		Map map = new Map(99, gameModel);
		BoardFrame boardFrame = new BoardFrame(gameModel);
		GraphicsBuilder graphicsBuilder = new GraphicsBuilder(map, boardFrame);
		graphicsBuilder.addElementsToGraphics();
	}

}
