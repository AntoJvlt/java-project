/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package view;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.KeyEvent;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import contract.Direction;
import contract.IGameController;
import contract.IGameModel;
import contract.IPlayerMouvementController;
import contract.IView;

/**
 * The Class View.
 * 
 * Handle the display of the game.
 */
public final class View implements IView, Runnable
{

	/** The view frame. */
	private BoardFrame viewFrame;

	/** The model. */
	private final IGameModel model;

	/** The player mouvement controller. */
	private IPlayerMouvementController playerMouvementController;

	/** The j option pane. */
	private JOptionPane jOptionPane = new JOptionPane();

	/** The zoom. */
	/* The zoom. */
	public static int ZOOM = 100;

	/** The width displayed. */
	public static int WIDTH_DISPLAYED = (int) (16 * IGameController.QUOTIENT_DISPLAYED_ELEMENTS);

	/** The height displayed. */
	public static int HEIGHT_DISPLAYED = (int) (9 * IGameController.QUOTIENT_DISPLAYED_ELEMENTS);

	/**
	 * Instantiates a new view.
	 *
	 * @param model the model
	 */
	public View(final IGameModel model)
	{
		this.model = model;
		SwingUtilities.invokeLater(this);
	}

	/**
	 * Key code to controller order.
	 *
	 * @param keyCode the key code
	 * @return the direction
	 */
	protected static Direction keyCodeToControllerOrder(final int keyCode)
	{
		switch (keyCode)
		{
		case KeyEvent.VK_Z:
			return Direction.UP;
		case KeyEvent.VK_Q:
			return Direction.LEFT;
		case KeyEvent.VK_S:
			return Direction.DOWN;
		case KeyEvent.VK_D:
			return Direction.RIGHT;
		default:
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run()
	{
		this.viewFrame = new BoardFrame("BoulderDash", model);
		this.getViewFrame().setController(this.playerMouvementController);
	}

	/* (non-Javadoc)
	 * @see contract.IView#setController(contract.IPlayerMouvementController)
	 */
	@Override
	public void setController(final IPlayerMouvementController playerMouvementController)
	{
		this.playerMouvementController = playerMouvementController;
	}

	/*
	 * (non-Javadoc)
	 * @see contract.IView#setDisplayFrame(java.awt.Rectangle)
	 */
	@Override
	public void updateCamera(Direction directionOrder)
	{
		this.getViewFrame().updateCamera(directionOrder);
	}

	/**
	 * Gets the view frame.
	 *
	 * @return the view frame
	 */
	public BoardFrame getViewFrame()
	{
		return this.viewFrame;
	}

	/* (non-Javadoc)
	 * @see contract.IView#showMessage(java.lang.String)
	 */
	@Override
	public void showMessage(String message)
	{
		JLabel label = new JLabel(message);
		label.setFont(new Font("Arial", Font.BOLD, 20));
		this.jOptionPane.showMessageDialog(null, label, "BoulderDash", JOptionPane.INFORMATION_MESSAGE);
	}

	/* (non-Javadoc)
	 * @see contract.IView#showReloadingMessage()
	 */
	@Override
	public void showReloadingMessage()
	{
		EventQueue.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				JLabel label = new JLabel("Reloading...");
				label.setFont(new Font("Arial", Font.BOLD, 20));
				jOptionPane.setMessage(label);
				JDialog dialog = jOptionPane.createDialog(null, "BoulderDash");
				dialog.setVisible(true);
			}
		});
	}

	/* (non-Javadoc)
	 * @see contract.IView#rebootCamera()
	 */
	@Override
	public void rebootCamera()
	{
		this.getViewFrame().getCamera().initCamera();
	}

	/* (non-Javadoc)
	 * @see contract.IView#centerCamera()
	 */
	@Override
	public void centerCamera()
	{
		this.getViewFrame().getCamera().centerCamera();
	}
}
