/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package view;

import java.awt.Rectangle;

import contract.Direction;
import contract.elements.IPlayer;

/**
 * The Class CameraFrame.
 * 
 * The area of the map wich is displayer in the game.
 * Move if the player goes to the border.
 */
public class CameraFrame extends Rectangle
{
	
	/** The no update area width. */
	private static int NO_UPDATE_AREA_WIDTH = 5;

	/** The no update area height. */
	private static int NO_UPDATE_AREA_HEIGHT = 5;

	/** The no update area. */
	private Rectangle noUpdateArea;

	/** The board frame. */
	private BoardFrame boardFrame;

	/**
	 * Instantiates a new camera frame.
	 *
	 * @param boardFrame the board frame
	 */
	public CameraFrame(BoardFrame boardFrame)
	{
		this.boardFrame = boardFrame;
		this.initCamera();
	}

	/**
	 * Update.
	 *
	 * @param directionOrder the direction order
	 */
	public void update(Direction directionOrder)
	{
		if (this.playerIsInUpdateArea())
		{
			this.moveCamera(directionOrder);
			this.moveNoUpdateArea();
		}
	}

	/**
	 * Inits the camera.
	 */
	public void initCamera()
	{
		this.setSize(View.WIDTH_DISPLAYED, View.HEIGHT_DISPLAYED);
		int posX = this.getPlayer().getPosX() - View.WIDTH_DISPLAYED / 2;
		int posY = this.getPlayer().getPosY() - View.HEIGHT_DISPLAYED / 2;

		this.setLocation(posX, posY);

		this.noUpdateArea = new Rectangle(0, 0, this.NO_UPDATE_AREA_WIDTH, this.NO_UPDATE_AREA_HEIGHT);
		this.moveNoUpdateArea();
	}

	/**
	 * Center camera.
	 */
	public void centerCamera()
	{
		this.setLocation(this.boardFrame.getModel().getMap().getMapWidth() / 2 - this.getSize().width / 2,
				this.boardFrame.getModel().getMap().getMapHeight() / 2 - this.getSize().height / 2);
	}

	/**
	 * Move camera.
	 *
	 * @param directionOrder the direction order
	 */
	private void moveCamera(Direction directionOrder)
	{
		switch (directionOrder)
		{
		case UP:
			this.setLocation((int) this.getLocation().getX(), (int) this.getLocation().getY() - 1);
			break;
		case RIGHT:
			this.setLocation((int) this.getLocation().getX() + 1, (int) this.getLocation().getY());
			break;
		case DOWN:
			this.setLocation((int) this.getLocation().getX(), (int) this.getLocation().getY() + 1);
			break;
		case LEFT:
			this.setLocation((int) this.getLocation().getX() - 1, (int) this.getLocation().getY());
			break;
		}
	}

	/**
	 * Move no update area.
	 */
	private void moveNoUpdateArea()
	{
		int noUpdateAreaPosX = (int) (this.getLocation().getX()
				+ (this.getWidth() / 2 - this.NO_UPDATE_AREA_WIDTH / 2));
		int noUpdateAreaPosY = (int) (this.getLocation().getY()
				+ (this.getHeight() / 2 - this.NO_UPDATE_AREA_HEIGHT / 2));

		this.noUpdateArea.setLocation(noUpdateAreaPosX, noUpdateAreaPosY);
	}

	/**
	 * Player is in update area.
	 *
	 * @return true, if successful
	 */
	private boolean playerIsInUpdateArea()
	{
		if (this.getPlayer().getPosX() < this.getNoUpdateArea().getLocation().getX() || this.getPlayer()
				.getPosX() > this.getNoUpdateArea().getLocation().getX() + this.getNoUpdateArea().width)
			return true;

		if (this.getPlayer().getPosY() < this.getNoUpdateArea().getLocation().getY() || this.getPlayer()
				.getPosY() > this.getNoUpdateArea().getLocation().getY() + this.getNoUpdateArea().height)
			return true;

		return false;
	}

	/**
	 * Gets the player.
	 *
	 * @return the player
	 */
	private IPlayer getPlayer()
	{
		return this.boardFrame.getModel().getPlayer();
	}

	/**
	 * Gets the no update area.
	 *
	 * @return the no update area
	 */
	public Rectangle getNoUpdateArea()
	{
		return this.noUpdateArea;
	}

	/**
	 * Sets the no update area.
	 *
	 * @param noUpdateArea the new no update area
	 */
	public void setNoUpdateArea(Rectangle noUpdateArea)
	{
		this.noUpdateArea = noUpdateArea;
	}

}
