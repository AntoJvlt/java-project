package view;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Observer;

import javax.swing.JFrame;

import contract.Direction;
import contract.IBoard;
import contract.IGameModel;
import contract.IPawn;
import contract.IPlayerMouvementController;
import contract.ISquare;

/**
 * <h1>The Class BoardFrame.</h1>
 * <p>
 * This class is just used to load the BoardPanel. It extends JPanel and
 * implements IBoard.
 * </p>
 * <p>
 * As the BoardPanel is a private class, BoardPanel is a Facade.
 * 
 * Extracted from the ShowBoard library provide in the project.
 * </p>
 *
 * @author Anne-Emilie DIET
 * @version 3.0
 * @see JFrame
 * @see BoardPanel
 * @see Dimension
 * @see Rectangle
 * @see IBoard
 * @see ISquare
 * @see IPawn
 */
public class BoardFrame extends JFrame implements IBoard, KeyListener
{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6563585351564617603L;

	/** The board panel. */
	private final BoardPanel boardPanel;

	/** The model. */
	private IGameModel model;

	/** The controller. */
	private IPlayerMouvementController playerMouvementController;

	/** The Camera. */
	private CameraFrame camera;

	/**
	 * Instantiates a new board frame.
	 *
	 * @param title     the title of the frame
	 * @param decorated the decorated
	 * @param model     the model
	 */
	public BoardFrame(final String title, final Boolean decorated, final IGameModel model)
	{
		super();

		this.model = model;
		this.camera = new CameraFrame(this);

		int width = this.getModel().getMap().getMapWidth();
		int height = this.getModel().getMap().getMapHeight();

		this.boardPanel = new BoardPanel(this.model);
		this.setDimension(new Dimension(width, height));
		this.getBoardPanel().setSquareGrill(this.model.getMap().getTabMap());
		this.setDisplayFrame(this.getCamera());

		this.setTitle(title);
		this.setSize(width * View.ZOOM, height * View.ZOOM);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setUndecorated(decorated);
		this.setContentPane(this.boardPanel);
		this.setResizable(false);
		this.addKeyListener(this);
		this.getModel().getObservable().addObserver(this.getObserver());

		this.setVisible(true);
	}

	/**
	 * Instantiates a new board frame.
	 *
	 * @param title the title
	 * @param model the model
	 */
	public BoardFrame(final String title, final IGameModel model)
	{
		this(title, false, model);
	}

	/**
	 * Instantiates a new board frame.
	 *
	 * @param model the model
	 */
	public BoardFrame(final IGameModel model)
	{
		this("", false, model);
	}

	/**
	 * Instantiates a new board frame.
	 *
	 * @param decorated the decorated
	 * @param model     the model
	 */
	public BoardFrame(final Boolean decorated, IGameModel model)
	{
		this("", decorated, model);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.exia.showboard.IBoard#getObserver()
	 */
	@Override
	public final Observer getObserver()
	{
		return this.getBoardPanel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.exia.showboard.IBoard#setDimension(java.awt.Dimension)
	 */
	@Override
	public final void setDimension(final Dimension dimension)
	{
		this.getBoardPanel().setDimension(dimension);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.exia.showboard.IBoard#getDimension()
	 */
	@Override
	public final Dimension getDimension()
	{
		return this.getBoardPanel().getDimension();
	}

	/**
	 * Gets the display frame.
	 *
	 * @return the display frame
	 */
	public final Rectangle getDisplayFrame()
	{
		return this.getBoardPanel().getDisplayFrame();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.exia.showboard.IBoard#setDisplayFrame(java.awt.Rectangle)
	 */
	@Override
	public final void setDisplayFrame(final Rectangle displayFrame)
	{
		this.getBoardPanel().setDisplayFrame(displayFrame);
	}

	/**
	 * Gets the board panel.
	 *
	 * @return the board panel
	 */
	private BoardPanel getBoardPanel()
	{
		return this.boardPanel;
	}

	/**
	 * Checks if is width looped.
	 *
	 * @return the boolean
	 */
	public final Boolean isWidthLooped()
	{
		return this.getBoardPanel().isWidthLooped();
	}

	/**
	 * Sets the width looped.
	 *
	 * @param widthLooped the new width looped
	 */
	public final void setWidthLooped(final Boolean widthLooped)
	{
		this.getBoardPanel().setWidthLooped(widthLooped);
	}

	/**
	 * Checks if is height looped.
	 *
	 * @return the boolean
	 */
	public final Boolean isHeightLooped()
	{
		return this.getBoardPanel().isHeightLooped();
	}

	/**
	 * Sets the height looped.
	 *
	 * @param heightLooped the new height looped
	 */
	public final void setHeightLooped(final Boolean heightLooped)
	{
		this.getBoardPanel().setHeightLooped(heightLooped);
	}

	/**
	 * Update the camera's position
	 *
	 * @param direction the movement direction
	 */
	public void updateCamera(Direction direction)
	{
		this.getCamera().update(direction);
	}

	public void addSquare(ISquare square, int x, int y)
	{
		this.getBoardPanel().addSquare(square, x, y);
	}

	/**
	 * Gets the camera.
	 *
	 * @return the camera
	 */
	public CameraFrame getCamera()
	{
		return this.camera;
	}

	/**
	 * Gets the model.
	 *
	 * @return the model
	 */
	public IGameModel getModel()
	{
		return this.model;
	}

	/**
	 * Gets the controller.
	 *
	 * @return the controller
	 */
	private IPlayerMouvementController getPlayerMouvementController()
	{
		return this.playerMouvementController;
	}

	/**
	 * Sets the controller.
	 *
	 * @param playerMouvementController the new controller
	 */
	protected void setController(final IPlayerMouvementController playerMouvementController)
	{
		this.playerMouvementController = playerMouvementController;
		this.getBoardPanel().setStatsDisplayer(new StatsDisplayer(playerMouvementController.getGameController(), this));
	}

	/*
	 * (non-Javadoc)
	 *ah
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyTyped(final KeyEvent e)
	{

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyPressed(final KeyEvent e)
	{
		this.getPlayerMouvementController().orderPerform(View.keyCodeToControllerOrder(e.getKeyCode()));
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyReleased(final KeyEvent e)
	{

	}
}
