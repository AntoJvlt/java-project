/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package view;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import contract.IGameController;

/**
 * The Class StatsDisplayer.
 * 
 * Handle the statistics displaying on the JPanel.
 */
public class StatsDisplayer
{

	/** The game controller. */
	private IGameController gameController;

	/** The board frame. */
	private BoardFrame boardFrame;

	/** The board image. */
	private BufferedImage boardImage;

	/** The clock image. */
	private BufferedImage clockImage;

	/** The diamond image. */
	private BufferedImage diamondImage;

	/** The trophy image. */
	private BufferedImage trophyImage;

	/** The open door image. */
	private BufferedImage openDoorImage;

	/** The close door image. */
	private BufferedImage closeDoorImage;

	/**
	 * Instantiates a new stats displayer.
	 *
	 * @param gameController the game controller
	 * @param boardFrame the board frame
	 */
	public StatsDisplayer(IGameController gameController, BoardFrame boardFrame)
	{
		this.gameController = gameController;
		this.boardFrame = boardFrame;
		this.init();
	}

	/**
	 * Inits the.
	 */
	private void init()
	{
		try
		{
			this.boardImage = ImageIO.read(this.getClass().getClassLoader().getResourceAsStream("Pictures/board.jpg"));
			this.boardImage = this.resizeImage(boardImage, this.getBoardFrame().getHeight() / 5, this.getBoardFrame().getWidth() / 5);

			this.clockImage = ImageIO.read(this.getClass().getClassLoader().getResourceAsStream("Pictures/clock.png"));
			this.diamondImage = ImageIO.read(this.getClass().getClassLoader().getResourceAsStream("Pictures/Diamond.png"));
			this.trophyImage = ImageIO.read(this.getClass().getClassLoader().getResourceAsStream("Pictures/trophy.png"));
			this.openDoorImage = ImageIO.read(this.getClass().getClassLoader().getResourceAsStream("Pictures/OpenDoor.png"));
			this.closeDoorImage = ImageIO.read(this.getClass().getClassLoader().getResourceAsStream("Pictures/CloseDoor.png"));

			int height = this.getBoardFrame().getHeight() / 28;
			int width = this.getBoardFrame().getWidth() / 45;

			int heightDoor = this.getBoardFrame().getHeight() / 22;
			int widthDoor = this.getBoardFrame().getWidth() / 39;

			this.clockImage = this.resizeImage(clockImage, height, width);
			this.diamondImage = this.resizeImage(diamondImage, height, width);
			this.trophyImage = this.resizeImage(trophyImage, height, width);
			this.closeDoorImage = this.resizeImage(closeDoorImage, heightDoor, widthDoor);
			this.openDoorImage = this.resizeImage(openDoorImage, heightDoor, widthDoor);

		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Draw stats.
	 *
	 * @param graphics the graphics
	 */
	public void drawStats(Graphics graphics)
	{
		/* Draw the board where are displayed the stats */
		graphics.drawImage(boardImage, this.getBoardFrame().getWidth() / 30, this.getBoardFrame().getHeight() / 30, null);

		graphics.setColor(Color.WHITE);
		graphics.setFont(new Font("Arial", Font.BOLD, this.getBoardFrame().getWidth() / 83));

		int textPosX = this.getBoardFrame().getWidth() / 20;
		
		/* Draw the time stats */
		String timeMessage = "Remaining time : " + this.getGameController().getRemainingTime() + " s";
		graphics.drawString(timeMessage, textPosX, this.getBoardFrame().getHeight() / 12);
		graphics.drawImage(clockImage, (int) (this.getBoardFrame().getWidth() / 5.5), (int) (this.getBoardFrame().getHeight() / 17.5), null);
		
		/* Draw the diamonds stats */
		String diamondMessage = "Diamonds collected : " + this.getGameController().getGameModel().getNumberDiamondCollected() + "/" + this.getGameController().getGameModel().getNumberDiamondTotal();
		graphics.drawString(diamondMessage, textPosX, (int) (this.getBoardFrame().getHeight() / 7.5));
		graphics.drawImage(diamondImage, this.getBoardFrame().getWidth() / 5, (int) (this.getBoardFrame().getHeight() / 9.4), null);

		/* Draw the score stats */
		String scoreMessage = "Score : " + this.getGameController().getCurrentScore();
		graphics.drawString(scoreMessage, textPosX, (int) (this.getBoardFrame().getHeight() / 5.5));
		graphics.drawImage(trophyImage, (int) (this.getBoardFrame().getWidth() / 7.9), (int) (this.getBoardFrame().getHeight() / 6.45), null);

		/* Draw the door open or close depending on it state */
		if (!this.getGameController().getGameModel().getMap().getDoor().isOpen())
			graphics.drawImage(closeDoorImage, (int) (this.getBoardFrame().getWidth() / 4.86), (int) (this.getBoardFrame().getHeight() / 5.6), null);
		else
			graphics.drawImage(openDoorImage, (int) (this.getBoardFrame().getWidth() / 4.86), (int) (this.getBoardFrame().getHeight() / 5.6), null);
	}

	/**
	 * Resize image.
	 *
	 * @param img the img
	 * @param height the height
	 * @param width the width
	 * @return the buffered image
	 */
	private BufferedImage resizeImage(BufferedImage img, int height, int width)
	{
		Image tmp = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		BufferedImage resized = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = resized.createGraphics();
		g2d.drawImage(tmp, 0, 0, null);
		g2d.dispose();
		return resized;
	}

	/**
	 * Gets the board image.
	 *
	 * @return the board image
	 */
	private BufferedImage getBoardImage()
	{
		return this.boardImage;
	}

	/**
	 * Gets the board frame.
	 *
	 * @return the board frame
	 */
	private BoardFrame getBoardFrame()
	{
		return this.boardFrame;
	}

	/**
	 * Gets the game controller.
	 *
	 * @return the game controller
	 */
	private IGameController getGameController()
	{
		return this.gameController;
	}
}
