/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.database;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import entity.MapPattern;

/**
 * The Class DAOMap.
 * 
 * Extract the map pattern from the data base.
 */
public class DAOMap extends DAOEntity<MapPattern>
{

	/**
	 * Instantiates a new DAO map.
	 *
	 * @param connection the connection
	 * @throws SQLException the SQL exception
	 */
	public DAOMap(final Connection connection) throws SQLException
	{
		super(connection);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see model.DAOEntity#create(model.Entity)
	 */
	@Override
	public boolean create(final MapPattern entity)
	{
		// Not implemented
		return false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see model.DAOEntity#delete(model.Entity)
	 */
	@Override
	public boolean delete(final MapPattern entity)
	{
		// Not implemented
		return false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see model.DAOEntity#update(model.Entity)
	 */
	@Override
	public boolean update(final MapPattern entity)
	{
		// Not implemented
		return false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see model.DAOEntity#find(int)
	 */
	@Override
	public MapPattern find(final int id) throws IOException
	{
		MapPattern mapPattern = null;

		try
		{
			// Call the stored procedure
			final String sql = "{call Map_boulderdashByLevel(?)}";
			final CallableStatement call = this.getConnection().prepareCall(sql);
			// Send parameter
			call.setInt(1, id);
			// Execute
			call.execute();
			final ResultSet resultSet = call.getResultSet();
			if (resultSet.first())
			{
				// Save the result in a string
				mapPattern = new MapPattern(resultSet.getString("map"));

			}
			return mapPattern;
		}
		catch (final SQLException e)
		{
			e.printStackTrace();
		}
		return null;
	}

}
