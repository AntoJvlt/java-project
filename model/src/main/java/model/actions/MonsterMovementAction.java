/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.actions;

import java.awt.Point;

import contract.Direction;
import contract.IGameController;
import contract.elements.ElementBehavior;
import contract.elements.IDoor;
import contract.elements.IElement;
import contract.elements.IKilleable;
import contract.elements.IPlayer;
import model.elements.Element;
import model.elements.mobiles.Monster;

/**
 * The Class MonsterMovementAction.
 * 
 * Handle the automatic movement of the monsters.
 */
public class MonsterMovementAction implements Runnable
{
	/** The monster. */
	Monster monster;

	/** The direction. */
	Direction direction;

	/** The movement thread. */
	Thread movementThread;

	/** The last direction. */
	Direction lastDirection;

	/**
	 * Instantiates a new monster movement action.
	 *
	 * @param monster the monster
	 */
	public MonsterMovementAction(Monster monster)
	{
		this.monster = monster;
		this.setDirection(Direction.RIGHT);
	}

	/**
	 * Stop.
	 */
	@SuppressWarnings("deprecation")
	public void stop()
	{
		if (this.movementThread != null)
			this.movementThread.stop();

		this.movementThread = null;
	}

	/**
	 * Start.
	 */
	public void start()
	{
		this.stop();
		this.movementThread = new Thread(this);
		this.movementThread.start();
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run()
	{
		while (true)
		{
			try
			{
				Thread.sleep(1200 - IGameController.MONSTER_SPEED);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}

			Point scanPlayer = this.scanForPlayerAround();

			if (scanPlayer != null)
			{
				this.getMonster().kill();
				this.getMonster().setPosition(scanPlayer);
			}
			else
			{
				Point nextPositionPoint = this.scanNextPosition();

				if (nextPositionPoint != null)
				{
					this.getMonster().setPosition(nextPositionPoint);

					if (this.getMonster().getMap().searchIpawnAtPos(nextPositionPoint.x, nextPositionPoint.y) instanceof IPlayer)
						((IKilleable) this.getMonster().getMap().getGameModel().getPlayer()).die("tué par un monstre");
				}
			}
		}
	}

	/**
	 * Scan next position.
	 *
	 * @return the point
	 */
	private Point scanNextPosition()
	{
		Point forwardPos = null;
		Point backwardPos = null;
		Point rightPos = null;
		Point leftPos = null;

		switch (this.getDirection())
		{
		case UP:
			forwardPos = this.isPosAvailable(new Point(this.getMonster().getX(), this.getMonster().getY() - 1));
			backwardPos = this.isPosAvailable(new Point(this.getMonster().getX(), this.getMonster().getY() + 1));
			rightPos = this.isPosAvailable(new Point(this.getMonster().getX() + 1, this.getMonster().getY()));
			leftPos = this.isPosAvailable(new Point(this.getMonster().getX() - 1, this.getMonster().getY()));
			break;
		case RIGHT:
			forwardPos = this.isPosAvailable(new Point(this.getMonster().getX() + 1, this.getMonster().getY()));
			backwardPos = this.isPosAvailable(new Point(this.getMonster().getX() - 1, this.getMonster().getY()));
			rightPos = this.isPosAvailable(new Point(this.getMonster().getX(), this.getMonster().getY() + 1));
			leftPos = this.isPosAvailable(new Point(this.getMonster().getX(), this.getMonster().getY() - 1));
			break;
		case DOWN:
			forwardPos = this.isPosAvailable(new Point(this.getMonster().getX(), this.getMonster().getY() + 1));
			backwardPos = this.isPosAvailable(new Point(this.getMonster().getX(), this.getMonster().getY() - 1));
			rightPos = this.isPosAvailable(new Point(this.getMonster().getX() - 1, this.getMonster().getY()));
			leftPos = this.isPosAvailable(new Point(this.getMonster().getX() + 1, this.getMonster().getY()));
			break;
		case LEFT:
			forwardPos = this.isPosAvailable(new Point(this.getMonster().getX() - 1, this.getMonster().getY()));
			backwardPos = this.isPosAvailable(new Point(this.getMonster().getX() + 1, this.getMonster().getY()));
			rightPos = this.isPosAvailable(new Point(this.getMonster().getX(), this.getMonster().getY() - 1));
			leftPos = this.isPosAvailable(new Point(this.getMonster().getX(), this.getMonster().getY() + 1));
			break;
		}

		if (leftPos == null)
		{
			if (rightPos != null)
			{
				this.setDirection(this.getNewDirection("right"));
				return rightPos;
			}
			else if (forwardPos != null)
			{
				this.setDirection(this.getNewDirection("forward"));
				return forwardPos;
			}
			else
			{
				this.setDirection(this.getNewDirection("backward"));
				return backwardPos;
			}
		}
		else
		{
			if(rightPos != null && leftPos != null && forwardPos != null & backwardPos != null)
			{
				this.setDirection(this.getNewDirection("forward"));
				return forwardPos;
			}
			else if(rightPos != null)
			{
				this.setDirection(this.getNewDirection("right"));
				return rightPos;
			}
			else if (forwardPos != null)
			{
				this.setDirection(this.getNewDirection("forward"));
				return forwardPos;
			}
			else
			{
				this.setDirection(this.getNewDirection("left"));
				return leftPos;
			}
		}
	}

	/**
	 * Gets the new direction.
	 *
	 * @param movementOrientation the movement orientation
	 * @return the new direction
	 */
	private Direction getNewDirection(String movementOrientation)
	{
		switch (this.getDirection())
		{
		case UP:
			switch (movementOrientation)
			{
			case "forward":
				return Direction.UP;
			case "right":
				return Direction.RIGHT;
			case "backward":
				return Direction.DOWN;
			case "left":
				return Direction.LEFT;
			}
			break;
		case RIGHT:
			this.lastDirection = Direction.RIGHT;
			switch (movementOrientation)
			{
			case "forward":
				return Direction.RIGHT;
			case "right":
				return Direction.DOWN;
			case "backward":
				return Direction.LEFT;
			case "left":
				return Direction.UP;
			}
			break;
		case DOWN:
			switch (movementOrientation)
			{
			case "forward":
				return Direction.DOWN;
			case "right":
				return Direction.LEFT;
			case "backward":
				return Direction.UP;
			case "left":
				return Direction.RIGHT;
			}
			break;
		case LEFT:
			this.lastDirection = Direction.LEFT;
			switch (movementOrientation)
			{
			case "forward":
				return Direction.LEFT;
			case "right":
				return Direction.UP;
			case "backward":
				return Direction.RIGHT;
			case "left":
				return Direction.DOWN;
			}
			break;
		}

		return this.getDirection();
	}

	/**
	 * Checks if is pos available.
	 *
	 * @param pos the pos
	 * @return the point
	 */
	private Point isPosAvailable(Point pos)
	{
		if (((Element) this.getMonster().getMap().getElementOnTheMap(pos.x, pos.y)).getBehavior() == ElementBehavior.PERMEABLE && !((this.getMonster().getMap().getElementOnTheMap(pos.x, pos.y)) instanceof IDoor))
		{
			if (this.getMonster().getMap().searchIpawnAtPos(pos.x, pos.y) != null)
			{
				if (((IElement) this.getMonster().getMap().searchIpawnAtPos(pos.x, pos.y)).getBehavior() == ElementBehavior.BLOCKING)
					return null;
			}
			return pos;
		}

		return null;
	}

	/**
	 * Scan for player around.
	 *
	 * @return the point
	 */
	private Point scanForPlayerAround()
	{
		Element upwardElement = this.getElementAtPos(new Point(this.getMonster().getX(), this.getMonster().getY() - 1));;
		Element bottomElement = this.getElementAtPos(new Point(this.getMonster().getX(), this.getMonster().getY() + 1));
		Element rightElement = this.getElementAtPos(new Point(this.getMonster().getX() + 1, this.getMonster().getY()));
		Element leftElement = this.getElementAtPos(new Point(this.getMonster().getX() - 1, this.getMonster().getY()));

		if (upwardElement instanceof IPlayer)
			return new Point(((IPlayer) upwardElement).getPosX(), ((IPlayer) upwardElement).getPosY());
		if (bottomElement instanceof IPlayer)
			return new Point(((IPlayer) bottomElement).getPosX(), ((IPlayer) bottomElement).getPosY());
		if (rightElement instanceof IPlayer)
			return new Point(((IPlayer) rightElement).getPosX(), ((IPlayer) rightElement).getPosY());
		if (leftElement instanceof IPlayer)
			return new Point(((IPlayer) leftElement).getPosX(), ((IPlayer) leftElement).getPosY());

		return null;
	}

	/**
	 * Gets the element at pos.
	 *
	 * @param pos the pos
	 * @return the element at pos
	 */
	private Element getElementAtPos(Point pos)
	{
		if (((Element) this.getMonster().getMap().getElementOnTheMap(pos.x, pos.y)).getBehavior() == ElementBehavior.PERMEABLE)
			if (this.getMonster().getMap().searchIpawnAtPos(pos.x, pos.y) != null)
				return (Element) this.getMonster().getMap().searchIpawnAtPos(pos.x, pos.y); // Return IPawn if it got found

		return (Element) this.getMonster().getMap().getElementOnTheMap(pos.x, pos.y); // Return ISquare if no IPawn have been returned
	}

	/**
	 * Sets the direction.
	 *
	 * @param direction the new direction
	 */
	private void setDirection(Direction direction)
	{
		this.direction = direction;
	}

	/**
	 * Gets the direction.
	 *
	 * @return the direction
	 */
	private Direction getDirection()
	{
		return this.direction;
	}

	/**
	 * Gets the monster.
	 *
	 * @return the monster
	 */
	private Monster getMonster()
	{
		return this.monster;
	}
}
