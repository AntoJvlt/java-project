/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.actions;

import java.util.ArrayList;

import contract.IGameController;
import contract.IMap;
import contract.IPawn;
import model.elements.mobiles.gravitables.GravitableElement;

/**
 * The Class TriggerCollapseAction.
 * 
 * The action used when elements trigger others elements
 */
public class TriggerCollapseAction implements Runnable
{
	/** The delay between triggers. */
	public static int DELAY_BETWEEN_TRIGGERS = 1300 - IGameController.COLLAPSING_SPEED;

	/** The elements to trigger. */
	private ArrayList<IPawn> elementsToTrigger = new ArrayList<>();

	/** The map. */
	private IMap map;

	/**
	 * Instantiates a new trigger collapse action.
	 *
	 * @param elementsToTrigger the elements to trigger
	 * @param map the map
	 */
	public TriggerCollapseAction(ArrayList<IPawn> elementsToTrigger, IMap map)
	{
		this.elementsToTrigger = elementsToTrigger;
		this.map = map;
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run()
	{
		for (int i = 0; i < this.elementsToTrigger.size(); i++)
		{
			IPawn element = this.elementsToTrigger.get(i);
			if (element instanceof GravitableElement)
				((GravitableElement) element).fall(this.getMap());
			
			try
			{
				Thread.sleep(TriggerCollapseAction.DELAY_BETWEEN_TRIGGERS);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}

	/**
	 * Gets the map.
	 *
	 * @return the map
	 */
	private IMap getMap()
	{
		return this.map;
	}
}
