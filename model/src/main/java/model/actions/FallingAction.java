/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.actions;

import java.awt.Point;
import java.util.ArrayList;

import contract.IGameController;
import contract.IMap;
import contract.IPawn;
import contract.ISquare;
import contract.elements.ElementBehavior;
import contract.elements.IDoor;
import contract.elements.IElement;
import contract.elements.IKilleable;
import contract.elements.IPlayer;
import contract.elements.ITriggering;
import model.elements.Element;
import model.elements.mobiles.gravitables.GravitableElement;
import model.sound.SoundManager;

/**
 * The Class FallingAction.
 * 
 * The action that does a gravitable element when it falls.
 */
public class FallingAction implements Runnable
{
	
	/** The delay time. */
	public static int DELAY_TIME = 1000 - IGameController.COLLAPSING_SPEED;

	/** The element. */
	private GravitableElement element;

	/** The map. */
	private IMap map;

	/** The is start. */
	private boolean isStart = false;

	/** The is end. */
	private boolean isEnd = false;

	/**
	 * Instantiates a new falling action.
	 *
	 * @param element the element
	 * @param map the map
	 */
	public FallingAction(GravitableElement element, IMap map)
	{
		this.setElement(element);
		this.map = map;
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run()
	{
		while (!this.isEnd() && !this.getMap().getGameModel().gameIsReloading())
		{
			try
			{
				Thread.sleep(FallingAction.DELAY_TIME);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}

			if (!this.getMap().getGameModel().gameIsReloading())
			{
				int xPos = this.getElement().getX();
				int yPos = this.getElement().getY();

				Point nextFallingPosition = this.scanNextFallingPosition();

				if (nextFallingPosition != null)
				{
					if (!this.isStart())
					{
						while (playerIsBlocking(nextFallingPosition))
						{
							try
							{
								Thread.sleep(20);
							}
							catch (InterruptedException e)
							{
								e.printStackTrace();
							}
						} ;

						this.start();
						this.getElement().setFalling(true);
					}

					IElement toKill = null;
					if (this.getElementAtPos(nextFallingPosition) instanceof IKilleable)
					{
						toKill = this.getElementAtPos(nextFallingPosition);
						((IKilleable) toKill).die("crushed");
					}

					ArrayList<IPawn> ipawnToTrigger = ITriggering.getIPawnsToTrigger(this.getElement(), this.getMap());

					this.getElement().setPosition(nextFallingPosition);

					this.getMap().getGameModel().updateChanges();

					if (toKill != null && toKill instanceof IPlayer)
						SoundManager.PlaySound(SoundManager.ROCK_KILL_SOUND);

					this.getElement().startTriggerAround(ipawnToTrigger, this.getMap());
				}
				else
				{
					this.getElement().setFalling(false);
					this.end();
				}
			}
		}
	}

	/**
	 * Check if a player is blocking the fall.
	 *
	 * @param nextFallingPosition the next falling position
	 * @return true, if successful
	 */
	private boolean playerIsBlocking(Point nextFallingPosition)
	{
		if (!this.getMap().getGameModel().gameIsReloading())
		{
			/* If the player is on the nextFallingPosition */
			if (this.getMap().searchIpawnAtPos(nextFallingPosition.x, nextFallingPosition.y) instanceof IPlayer)
				return true;

			/* If the player is blocking the right bottom fall */
			if (nextFallingPosition.x == this.getElement().getX() + 1 && nextFallingPosition.y == this.getElement().getY() + 1)
				if (this.getMap().searchIpawnAtPos(this.getElement().getX() + 1, this.getElement().getY()) instanceof IPlayer)
					return true;

			/* If the player is blocking the left bottom fall */
			if (nextFallingPosition.x == this.getElement().getX() - 1 && nextFallingPosition.y == this.getElement().getY() + 1)
				if (this.getMap().searchIpawnAtPos(this.getElement().getX() - 1, this.getElement().getY()) instanceof IPlayer)
					return true;
		}
		else
		{
			return true;
		}

		return false;
	}

	/**
	 * Scan next falling position.
	 *
	 * @return the point
	 */
	private Point scanNextFallingPosition()
	{
		Element bottomElement = this.getElementAtPos(new Point(this.getElement().getX(), this.getElement().getY() + 1));

		Element rightElement = this.getElementAtPos(new Point(this.getElement().getX() + 1, this.getElement().getY()));
		Element lowerRightElement = this.getElementAtPos(new Point(this.getElement().getX() + 1, this.getElement().getY() + 1));

		Element leftElement = this.getElementAtPos(new Point(this.getElement().getX() - 1, this.getElement().getY()));
		Element lowerLeftElement = this.getElementAtPos(new Point(this.getElement().getX() - 1, this.getElement().getY() + 1));

		/* Return bottom position if it's available */
		if (this.elementLocationIsAvailable(bottomElement))
			return new Point(this.getElement().getX(), this.getElement().getY() + 1);

		/* Return lower right position if it's available */
		if (this.elementLocationIsAvailable(lowerRightElement))
			/* Check if the element can roll on the lower right position */
			if (this.elementLocationIsAvailable(rightElement) && bottomElement instanceof GravitableElement)
				return new Point(this.getElement().getX() + 1, this.getElement().getY() + 1);

		/* Return lower left position if it's available */
		if (this.elementLocationIsAvailable(lowerLeftElement))
			/* Check if the element can roll on the lower left position */
			if (this.elementLocationIsAvailable(leftElement) && bottomElement instanceof GravitableElement)
				return new Point(this.getElement().getX() - 1, this.getElement().getY() + 1);

		return null; // Return null if no falling position has been found
	}

	/**
	 * Check if Element location is available.
	 *
	 * @param elem the elem
	 * @return true, if successful
	 */
	private boolean elementLocationIsAvailable(Element elem)
	{
		if (elem == null)
			return false;

		if (elem instanceof ISquare && elem.getBehavior() == ElementBehavior.PERMEABLE && !(elem instanceof IDoor))
			return true;
		else
			return false;
	}

	/**
	 * Gets the element at pos.
	 *
	 * @param pos the pos
	 * @return the element at pos
	 */
	private Element getElementAtPos(Point pos)
	{
		if (((Element) this.getMap().getElementOnTheMap(pos.x, pos.y)).getBehavior() == ElementBehavior.PERMEABLE)
			if (this.getMap().searchIpawnAtPos(pos.x, pos.y) != null)
				return (Element) this.getMap().searchIpawnAtPos(pos.x, pos.y); // Return IPawn if it got found

		return (Element) this.getMap().getElementOnTheMap(pos.x, pos.y); // Return ISquare if no IPawn have been returned
	}

	/**
	 * Gets the element.
	 *
	 * @return the element
	 */
	private GravitableElement getElement()
	{
		return this.element;
	}

	/**
	 * Sets the element.
	 *
	 * @param element the new element
	 */
	private void setElement(GravitableElement element)
	{
		this.element = element;
	}

	/**
	 * Gets the map.
	 *
	 * @return the map
	 */
	private IMap getMap()
	{
		return this.map;
	}

	/**
	 * Checks if is end.
	 *
	 * @return true, if is end
	 */
	private boolean isEnd()
	{
		return this.isEnd;
	}

	/**
	 * End.
	 */
	private void end()
	{
		this.isEnd = true;
	}

	/**
	 * Checks if is start.
	 *
	 * @return true, if is start
	 */
	private boolean isStart()
	{
		return this.isStart;
	}

	/**
	 * Start.
	 */
	private void start()
	{
		this.isStart = true;
	}

}
