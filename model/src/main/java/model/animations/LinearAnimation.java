/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.animations;

import java.util.HashMap;

import contract.IGameModel;
import model.elements.AnimationSpritesManager;
import model.elements.Element;

/**
 * The Class LinearAnimation.
 * 
 * Create a linearAnimation for multiples elements, it used their SpritesManagers to animate the element.
 */
public class LinearAnimation implements Runnable
{

	/** The animations. */
	private HashMap<Element, AnimationSpritesManager> animations = new HashMap<>();

	/** The delay. */
	private final int DELAY;

	/** The is end. */
	private boolean isEnd = false;

	/** The game manager. */
	private IGameModel gameManager;

	/** The animation thread. */
	private Thread animationThread;

	/**
	 * Instantiates a new linear animation.
	 *
	 * @param delay the delay
	 * @param gameManager the game manager
	 */
	public LinearAnimation(int delay, IGameModel gameManager)
	{
		this.DELAY = delay;
		this.gameManager = gameManager;

		this.animationThread = new Thread(this);
		animationThread.start();
	}

	/*
	 * Change the state of all elements and update the view each tick
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run()
	{
		while (!isEnd)
		{
			this.getAnimations().forEach((k, v) ->
			{
				v.nextAnimationStep();
				k.setActiveSprite(v.get(v.getCurrentIndex()));
			});

			if (!this.gameManager.gameIsReloading())
				this.gameManager.updateChanges();

			try
			{
				Thread.sleep(this.DELAY);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}

		this.getAnimations().forEach((k, v) -> k.setActiveSprite(v.get(0)));
	}

	/**
	 * Gets the animations.
	 *
	 * @return the animations
	 */
	public HashMap<Element, AnimationSpritesManager> getAnimations()
	{
		return this.animations;
	}

	/**
	 * Adds the animation.
	 *
	 * @param element the element
	 * @param animationSpritesManager the animation sprites manager
	 */
	public void addAnimation(Element element, AnimationSpritesManager animationSpritesManager)
	{
		this.getAnimations().put(element, animationSpritesManager);
	}

	/**
	 * Checks if is end.
	 *
	 * @return true, if is end
	 */
	public boolean isEnd()
	{
		return this.isEnd;
	}

	/**
	 * Stop.
	 */
	public void stop()
	{
		this.isEnd = true;
	}

	/**
	 * Clear.
	 */
	@SuppressWarnings("deprecation")
	public void clear()
	{
		this.animationThread.stop();
	}

}
