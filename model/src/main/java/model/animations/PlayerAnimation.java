/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.animations;

import contract.Direction;
import contract.IGameController;
import contract.IGameModel;
import model.elements.mobiles.Player;

/**
 * The Class PlayerAnimation.
 * 
 * Animate the player with his multiples LinearAnimations for each direction.
 */
public class PlayerAnimation
{
	
	/** The mooving animation delay. */
	private final int MOOVING_ANIMATION_DELAY = 200 - IGameController.PLAYER_MOVEMENT_SPEED;

	/** The waiting animation delay. */
	private final int WAITING_ANIMATION_DELAY = 1000;

	/** The movement direction. */
	private Direction movementDirection;

	/** The game manager. */
	private IGameModel gameManager;

	/** The player. */
	private Player player;

	/** The countdown before waiting. */
	private CountdownBeforeWaiting countdownBeforeWaiting = new CountdownBeforeWaiting();

	/** The moving action record. */
	private MovingActionRecord movingActionRecord = new MovingActionRecord();

	/** The waiting animation. */
	private LinearAnimation waitingAnimation;

	/** The up deplacement animation. */
	private LinearAnimation upDeplacementAnimation;

	/** The right deplacement animation. */
	private LinearAnimation rightDeplacementAnimation;

	/** The down deplacement animation. */
	private LinearAnimation downDeplacementAnimation;

	/** The left deplacement animation. */
	private LinearAnimation leftDeplacementAnimation;

	/** The current running animation. */
	private LinearAnimation currentRunningAnimation;

	/**
	 * Instantiates a new player animation.
	 *
	 * @param player the player
	 * @param gameManager the game manager
	 */
	public PlayerAnimation(Player player, IGameModel gameManager)
	{
		this.gameManager = gameManager;
		this.player = player;

		this.startWaitingAnimation();
	}

	/**
	 * Update movement action.
	 *
	 * @param direction the direction
	 */
	public void updateMovementAction(Direction direction)
	{
		this.movementDirection = direction;
		this.playMovementAnimation();

		this.getMovingActionRecord().startRecord();

		if (this.getCountdownBeforeWaiting().isCounting())
			this.getCountdownBeforeWaiting().stopCounting();
	}

	/**
	 * Play movement animation.
	 */
	private void playMovementAnimation()
	{
		switch (this.movementDirection)
		{
		case UP:
			if (this.currentRunningAnimation != this.upDeplacementAnimation || this.upDeplacementAnimation.isEnd())
			{
				if (this.currentRunningAnimation != null)
					this.currentRunningAnimation.stop();

				this.upDeplacementAnimation = new LinearAnimation(this.MOOVING_ANIMATION_DELAY, this.getGameManager());
				this.upDeplacementAnimation.addAnimation(this.getPlayer(),
						this.getPlayer().getUpAnimationSpritesManager());
				this.setCurrentRunningAnimation(this.upDeplacementAnimation);
			}
			break;
		case RIGHT:
			if (this.currentRunningAnimation != this.rightDeplacementAnimation
					|| this.rightDeplacementAnimation.isEnd())
			{
				if (this.currentRunningAnimation != null)
					this.currentRunningAnimation.stop();

				this.rightDeplacementAnimation = new LinearAnimation(this.MOOVING_ANIMATION_DELAY,
						this.getGameManager());
				this.rightDeplacementAnimation.addAnimation(this.getPlayer(),
						this.getPlayer().getRightAnimationSpritesManager());
				this.setCurrentRunningAnimation(this.rightDeplacementAnimation);
			}
			break;
		case DOWN:
			if (this.currentRunningAnimation != this.downDeplacementAnimation || this.downDeplacementAnimation.isEnd())
			{
				if (this.currentRunningAnimation != null)
					this.currentRunningAnimation.stop();

				this.downDeplacementAnimation = new LinearAnimation(this.MOOVING_ANIMATION_DELAY,
						this.getGameManager());
				this.downDeplacementAnimation.addAnimation(this.getPlayer(),
						this.getPlayer().getDownAnimationSpritesManager());
				this.setCurrentRunningAnimation(this.downDeplacementAnimation);
			}
			break;
		case LEFT:
			if (this.currentRunningAnimation != this.leftDeplacementAnimation || this.leftDeplacementAnimation.isEnd())
			{
				if (this.currentRunningAnimation != null)
					this.currentRunningAnimation.stop();

				this.leftDeplacementAnimation = new LinearAnimation(this.MOOVING_ANIMATION_DELAY,
						this.getGameManager());
				this.leftDeplacementAnimation.addAnimation(this.getPlayer(),
						this.getPlayer().getLeftAnimationSpritesManager());
				this.setCurrentRunningAnimation(this.leftDeplacementAnimation);
			}
			break;
		}
	}

	/**
	 * Start waiting animation.
	 */
	private void startWaitingAnimation()
	{
		this.stopCurrentRunningAnimation();

		if (this.waitingAnimation != null)
			this.waitingAnimation.stop();

		this.waitingAnimation = new LinearAnimation(this.WAITING_ANIMATION_DELAY, this.getGameManager());
		this.waitingAnimation.addAnimation(this.getPlayer(), this.getPlayer().getWaitingAnimationSpritesManager());
		this.setCurrentRunningAnimation(this.waitingAnimation);
	}

	/**
	 * Stop current running animation.
	 */
	private void stopCurrentRunningAnimation()
	{
		if (this.getCurrentRunningAnimation() != null)
			this.getCurrentRunningAnimation().stop();
	}
	
	/**
	 * Clear.
	 */
	public void clear()
	{
		if(this.getCurrentRunningAnimation() != null)
			this.getCurrentRunningAnimation().clear();
	}

	/**
	 * Gets the player.
	 *
	 * @return the player
	 */
	private Player getPlayer()
	{
		return this.player;
	}

	/**
	 * Gets the game manager.
	 *
	 * @return the game manager
	 */
	private IGameModel getGameManager()
	{
		return this.gameManager;
	}

	/**
	 * Gets the current running animation.
	 *
	 * @return the current running animation
	 */
	private LinearAnimation getCurrentRunningAnimation()
	{
		return currentRunningAnimation;
	}

	/**
	 * Sets the current running animation.
	 *
	 * @param currentRunningAnimation the new current running animation
	 */
	private void setCurrentRunningAnimation(LinearAnimation currentRunningAnimation)
	{
		this.currentRunningAnimation = currentRunningAnimation;
	}

	/**
	 * Gets the countdown before waiting.
	 *
	 * @return the countdown before waiting
	 */
	private CountdownBeforeWaiting getCountdownBeforeWaiting()
	{
		return this.countdownBeforeWaiting;
	}

	/**
	 * Gets the moving action record.
	 *
	 * @return the moving action record
	 */
	private MovingActionRecord getMovingActionRecord()
	{
		return this.movingActionRecord;
	}

	/**
	 * The Class CountdownBeforeWaiting.
	 * 
	 * Count delay when the player don't move, so it's possible to start the waiting animation when he is waiting.
	 */
	class CountdownBeforeWaiting implements Runnable
	{
		
		/** The Timer. */
		private int Timer;

		/** The is counting. */
		private boolean isCounting = false;

		/** The count thread. */
		private Thread countThread;

		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run()
		{
			while (isCounting)
			{
				try
				{
					Thread.sleep(1000);
				} catch (InterruptedException e)
				{
					e.printStackTrace();
				}

				if (Timer-- <= 0)
					this.isCounting = false;
			}
			startWaitingAnimation();
		}

		/**
		 * Start counting.
		 */
		public void startCounting()
		{
			this.isCounting = true;
			this.Timer = 0;

			if (this.countThread == null || !this.countThread.isAlive())
			{
				this.countThread = new Thread(this);
				this.countThread.start();
			}
		}

		/**
		 * Stop counting.
		 */
		@SuppressWarnings("deprecation")
		public void stopCounting()
		{
			this.isCounting = false;

			if (this.countThread != null & this.countThread.isAlive())
			{
				this.countThread.stop();
			}
		}

		/**
		 * Checks if is counting.
		 *
		 * @return true, if is counting
		 */
		public boolean isCounting()
		{
			return this.isCounting;
		}
	}

	/**
	 * The Class MovingActionRecord.
	 * 
	 * Record when the player move, so it's possible to know if they player is actually running without stopping.
	 */
	class MovingActionRecord implements Runnable
	{
		
		/** The Constant MOOVING_RECORD_DELAY. */
		private static final int MOOVING_RECORD_DELAY = 200 - IGameController.PLAYER_MOVEMENT_SPEED;

		/** The record thread. */
		private Thread recordThread;

		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public void run()
		{
			try
			{
				Thread.sleep(MOOVING_RECORD_DELAY);
			} catch (InterruptedException e)
			{
				e.printStackTrace();
			}

			getCurrentRunningAnimation().stop();
			getCountdownBeforeWaiting().startCounting();
		}

		/**
		 * Start record.
		 */
		@SuppressWarnings("deprecation")
		public void startRecord()
		{
			if (this.recordThread != null && this.recordThread.isAlive())
				this.recordThread.stop();

			recordThread = new Thread(this);
			recordThread.start();
		}
	}
}
