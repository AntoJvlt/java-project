/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Observable;

import contract.IGameController;
import contract.IGameModel;
import contract.IMap;
import contract.IPawn;
import contract.elements.IPlayer;
import model.animations.LinearAnimation;
import model.elements.Element;
import model.elements.mobiles.Monster;
import model.elements.mobiles.Player;
import model.elements.mobiles.gravitables.Diamond;
import model.elements.mobiles.gravitables.GravitableElement;
import model.elements.mobiles.gravitables.Rock;
import model.sound.SoundManager;

/**
 * The Class GameModel.
 * 
 * The core of the game's data, manage all the elements and animations.
 * Observed by the view.
 */
public final class GameModel extends Observable implements IGameModel
{
	/** The map. */
	private Map map;

	/** The player. */
	private Player player;

	/** The environnement static animation. */
	private LinearAnimation environnementStaticAnimation;

	/** The player won. */
	private boolean playerWon = false;

	/** The game is reloading. */
	private boolean gameIsReloading = false;

	/**
	 * Instantiates a new game model.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public GameModel() throws IOException
	{
		this.loadMap();
		this.loadPlayer();
		this.startAnimations();
		this.triggerAllFlyingElements();
		SoundManager.PlaySound(SoundManager.START_SOUND);
	}

	/**
	 * Load player.
	 *
	 * @return true, if successful
	 */
	private boolean loadPlayer()
	{
		for (int i = 0; i < this.getMap().getMobilesElements().size(); i++)
		{
			if (this.getMap().getMobilesElements().get(i) instanceof Player)
			{
				this.player = (Player) this.getMap().getMobilesElements().get(i);
				break;
			}
		}

		return true;
	}

	/**
	 * Trigger all flying elements.
	 *
	 * @return true, if successful
	 */
	private boolean triggerAllFlyingElements()
	{
		for (IPawn element : this.getMap().getMobilesElements())
		{
			if (element instanceof GravitableElement)
				((GravitableElement) element).fall(this.getMap());
		}

		return true;
	}

	/**
	 * Start animations.
	 *
	 * @return true, if successful
	 */
	public boolean startAnimations()
	{
		this.environnementStaticAnimation = new LinearAnimation(120, this);

		for (IPawn element : this.getMap().getMobilesElements())
		{
			if (element instanceof Rock)
				environnementStaticAnimation.addAnimation((Element) element, ((Rock) element).getMainAnimationSpritesManager());
			else if (element instanceof Diamond)
				environnementStaticAnimation.addAnimation((Element) element, ((Diamond) element).getMainAnimationSpritesManager());
			else if (element instanceof Monster)
			{
				environnementStaticAnimation.addAnimation((Element) element, ((Monster) element).getMainAnimationsSpritesManager());
				((Monster) element).startMoving();
			}
		}

		this.getPlayer().startAnimation();

		return true;
	}

	/**
	 * Stop animation.
	 *
	 * @return true, if successful
	 */
	private boolean stopAnimation()
	{
		this.environnementStaticAnimation.clear();
		this.getPlayer().stopAnimation();

		for (IPawn element : this.getMap().getMobilesElements())
			if (element instanceof Monster)
				((Monster) element).stopMoving();

		return true;
	}

	/* (non-Javadoc)
	 * @see contract.IGameModel#resetGame()
	 */
	@Override
	public boolean resetGame()
	{
		this.gameIsReloading = true;
		this.playerWon = false;
		Diamond.nbrDiamondTotal = 0; // Reset diamond count
		Diamond.nbrDiamondCollected = 0;

		while (!this.stopAnimation());

		while (!((Map) this.getMap()).reset()); // Reset the map

		while (!this.loadPlayer());

		while (!this.startAnimations());

		while (!this.triggerAllFlyingElements());

		this.gameIsReloading = false;

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see contract.IModel#getLevel()
	 */
	@Override
	public IMap getMap()
	{
		return this.map;
	}

	/**
	 * Sets the map.
	 *
	 * @param map the new map
	 */
	private void setMap(final Map map)
	{
		this.map = map;
		this.setChanged();
		this.notifyObservers();

	}

	/**
	 * Load map.
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see contract.IModel#loadLevel()
	 */
	private void loadMap()
	{
		try
		{
			map = new Map(IGameController.NUMERO_MAP, this);
		}
		catch (IOException | SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see contract.IModel#getObservable()
	 */
	@Override
	public Observable getObservable()
	{
		return this;
	}

	/* (non-Javadoc)
	 * @see contract.IGameModel#updateChanges()
	 */
	@Override
	public void updateChanges()
	{
		if (this.getMap() != null && !this.getMap().isBeingModified())
		{
			this.setChanged();
			this.notifyObservers();
		}
	}

	/* (non-Javadoc)
	 * @see contract.IGameModel#getPlayer()
	 */
	@Override
	public IPlayer getPlayer()
	{
		return this.player;
	}

	/* (non-Javadoc)
	 * @see contract.IGameModel#getNumberDiamondCollected()
	 */
	@Override
	public int getNumberDiamondCollected()
	{
		return Diamond.nbrDiamondCollected;
	}

	/* (non-Javadoc)
	 * @see contract.IGameModel#playerWon()
	 */
	@Override
	public boolean playerWon()
	{
		return this.playerWon;
	}

	/* (non-Javadoc)
	 * @see contract.IGameModel#setPlayerWon()
	 */
	@Override
	public void setPlayerWon()
	{
		SoundManager.PlaySound(SoundManager.WIN_SOUND);
		this.playerWon = true;
	}

	/* (non-Javadoc)
	 * @see contract.IGameModel#getNumberDiamondTotal()
	 */
	@Override
	public int getNumberDiamondTotal()
	{
		return Diamond.nbrDiamondTotal;
	}

	/* (non-Javadoc)
	 * @see contract.IGameModel#gameIsReloading()
	 */
	@Override
	public boolean gameIsReloading()
	{
		return this.gameIsReloading;
	}
}
