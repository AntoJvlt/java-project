/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.elements;

import java.awt.Image;
import java.io.IOException;

import contract.IMap;
import contract.ISquare;
import contract.elements.ElementBehavior;
import contract.elements.IElement;

/**
 * The Class Element.
 * 
 */
public abstract class Element implements IElement, ISquare
{

	/** The active sprite. */
	private Sprite activeSprite;

	/** The behavior. */
	private ElementBehavior behavior;

	/** The level. */
	private IMap level;

	/**
	 * Instantiates a new element.
	 *
	 * @param level the level
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public Element(IMap level) throws IOException
	{
		this.setLevel(level);
	}

	/**
	 * Gets the active sprite.
	 *
	 * @return the active sprite
	 */
	public Sprite getActiveSprite()
	{
		return this.activeSprite;
	}

	/* (non-Javadoc)
	 * @see contract.ISquare#getImage()
	 */
	@Override
	public Image getImage()
	{
		return this.activeSprite.getImage();
	}

	/* (non-Javadoc)
	 * @see contract.elements.IElement#getBehavior()
	 */
	@Override
	public ElementBehavior getBehavior()
	{
		return this.behavior;
	}

	/**
	 * Sets the behavior.
	 *
	 * @param behavior the new behavior
	 */
	protected void setBehavior(ElementBehavior behavior)
	{
		this.behavior = behavior;
	}

	/**
	 * Sets the active sprite.
	 *
	 * @param sprite the new active sprite
	 */
	public void setActiveSprite(Sprite sprite)
	{
		this.activeSprite = sprite;
	}

	/* (non-Javadoc)
	 * @see contract.elements.IElement#getMap()
	 */
	@Override
	public IMap getMap()
	{
		return level;
	}

	/**
	 * Sets the level.
	 *
	 * @param level the new level
	 */
	public void setLevel(IMap level)
	{
		this.level = level;
	}

	/* (non-Javadoc)
	 * @see contract.elements.IElement#execCollisionAction()
	 */
	@Override
	public abstract void execCollisionAction();
}