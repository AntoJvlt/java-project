/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.elements;

import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

/**
 * The Class Sprite.
 */
public class Sprite
{

	/** The image name. */
	private String imageName;

	/** The image. */
	private Image image;

	/**
	 * Instantiates a new sprite.
	 *
	 * @param imageName the image name
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public Sprite(final String imageName) throws IOException
	{
		this.setImageName(imageName);
		this.loadImage();

	}

	/**
	 * Gets the image.
	 *
	 * @return the image
	 */
	public final Image getImage()
	{
		return this.image;
	}

	/**
	 * Load image.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public final void loadImage() throws IOException
	{
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("Pictures/" + this.getImageName());
		this.setImage(ImageIO.read(inputStream));
	}

	/**
	 * Sets the image.
	 *
	 * @param image the new image
	 */
	private void setImage(final Image image)
	{
		this.image = image;
	}

	/**
	 * Gets the image name.
	 *
	 * @return the image name
	 */
	private String getImageName()
	{
		return this.imageName;
	}

	/**
	 * Sets the image name.
	 *
	 * @param imageName the new image name
	 */
	private void setImageName(final String imageName)
	{
		this.imageName = imageName;
	}

}