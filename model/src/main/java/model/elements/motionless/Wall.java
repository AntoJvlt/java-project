/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.elements.motionless;

import java.io.IOException;

import contract.IMap;
import contract.elements.ElementBehavior;
import model.elements.Sprite;

/**
 * The Class Wall.
 */
public class Wall extends MotionlessElement
{

	/** The wall sprite. */
	private final Sprite wallSprite = new Sprite("Wall3.png");

	/**
	 * Instantiates a new wall.
	 *
	 * @param level the level
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public Wall(IMap level) throws IOException
	{
		super(level);
		this.setBehavior(ElementBehavior.BLOCKING);
		this.setActiveSprite(wallSprite);
	}

	/* (non-Javadoc)
	 * @see model.elements.Element#execCollisionAction()
	 */
	@Override
	public void execCollisionAction()
	{
		// TODO Auto-generated method stub	
	}
}
