/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */

package model.elements.motionless;

import java.io.IOException;

import contract.IMap;
import contract.elements.ElementBehavior;
import contract.elements.IDiggable;
import model.elements.Sprite;
import model.sound.SoundManager;

/**
 * The Class Dirt.
 */
public class Dirt extends MotionlessElement implements IDiggable
{

	/** The dirt sprite. */
	private final Sprite dirtSprite = new Sprite("Dirt.png");

	/** The dirt dig sprite. */
	private final Sprite dirtDigSprite = new Sprite("DirtDig.png");

	/** The dug. */
	private boolean dug = false;

	/**
	 * Instantiates a new dirt.
	 *
	 * @param isDug the is dug
	 * @param level the level
	 * @throws IOException Signals that an I/O exception has occurred.
	 */

	public Dirt(boolean isDug, IMap level) throws IOException
	{
		super(level);
		if (isDug)
		{
			this.dug = true;
			this.setActiveSprite(this.dirtDigSprite);
			this.setBehavior(ElementBehavior.PERMEABLE);
		}
		else
		{
			this.setActiveSprite(dirtSprite);
			this.setBehavior(ElementBehavior.BLOCKING);
		}
	}

	/**
	 * Checks if is dug.
	 *
	 * @return true, if is dug
	 */
	public boolean isDug()
	{
		return this.dug;
	}

	/* (non-Javadoc)
	 * @see contract.elements.IDiggable#dig()
	 */
	@Override
	public void dig()
	{
		SoundManager.PlaySound(SoundManager.DIRT_DIG_SOUND);
		if (this.isNotDug())
		{
			this.dug = true;
			this.setActiveSprite(this.dirtDigSprite);
			this.setBehavior(ElementBehavior.PERMEABLE);
		}
	}

	/**
	 * Checks if is not dug.
	 *
	 * @return true, if is not dug
	 */
	public boolean isNotDug()
	{
		if (this.isDug())
			return false;
		else
			return true;
	}

	/**
	 * Not dug.
	 */
	public void notDug()
	{
		if (this.isDug())
		{
			this.dug = false;
		}
	}

	/* (non-Javadoc)
	 * @see model.elements.Element#execCollisionAction()
	 */
	@Override
	public void execCollisionAction()
	{
		if (this.isNotDug())
			this.dig();
	}
}