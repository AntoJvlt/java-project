/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.elements.motionless;

import java.io.IOException;

import contract.IMap;
import model.elements.Element;

/**
 * The Class MotionlessElement.
 */
public abstract class MotionlessElement extends Element
{

	/**
	 * Instantiates a new motionless element.
	 *
	 * @param level the level
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public MotionlessElement(IMap level) throws IOException
	{
		super(level);
	}
}