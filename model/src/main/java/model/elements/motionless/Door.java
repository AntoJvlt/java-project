/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */

package model.elements.motionless;

import java.io.IOException;

import contract.IMap;
import contract.IPawn;
import contract.elements.ElementBehavior;
import contract.elements.IDoor;
import model.elements.Sprite;
import model.sound.SoundManager;

/**
 * The Class Door.
 */
public class Door extends MotionlessElement implements IDoor
{

	/** The close door sprite. */
	private final Sprite closeDoorSprite = new Sprite("CloseDoor.png");

	/** The open door sprite. */
	private final Sprite openDoorSprite = new Sprite("OpenDoor.png");

	/** The open. */
	private boolean open = false;

	/**
	 * Instantiates a new door.
	 *
	 * @param level the level
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public Door(IMap level) throws IOException
	{
		super(level);
		this.setBehavior(ElementBehavior.PERMEABLE);
		this.setActiveSprite(this.closeDoorSprite);
	}

	/* (non-Javadoc)
	 * @see contract.elements.IDoor#isOpen()
	 */
	@Override
	public boolean isOpen()
	{
		return this.open;
	}

	/* (non-Javadoc)
	 * @see contract.elements.IDoor#open()
	 */
	@Override
	public void open()
	{
		SoundManager.PlaySound(SoundManager.OPEN_DOOR_SOUND);
		if (!this.isOpen())
		{
			this.open = true;
			this.setActiveSprite(openDoorSprite);
		}
	}

	/**
	 * Checks if is close.
	 *
	 * @return true, if is close
	 */
	public boolean isClose()
	{
		if (this.isOpen())
			return false;
		else
			return true;
	}

	/* (non-Javadoc)
	 * @see model.elements.Element#execCollisionAction()
	 */
	@Override
	public void execCollisionAction()
	{
		if (this.isOpen())
		{
			this.getMap().destroyIpawn((IPawn) getMap().getGameModel().getPlayer());
			this.getMap().getGameModel().setPlayerWon();
		}
	}

}