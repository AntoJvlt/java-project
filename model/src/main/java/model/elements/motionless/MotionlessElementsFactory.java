/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.elements.motionless;

import java.io.IOException;

import contract.IMap;

/**
 * A factory for creating MotionlessElements objects.
 */
public final class MotionlessElementsFactory
{

	/** The Constant DIRT. */
	public final static int DIRT = 1;

	/** The Constant DIRTDIG. */
	public final static int DIRTDIG = 2;

	/** The Constant DOOR. */
	public final static int DOOR = 3;

	/** The Constant WALL. */
	public final static int WALL = 4;

	/**
	 * Creates a new MotionlessElements object.
	 *
	 * @param motionlessElement the motionless element
	 * @param level the level
	 * @return the motionless element
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static final MotionlessElement createMotionlessElement(final int motionlessElement, IMap level) throws IOException
	{
		switch (motionlessElement)
		{
		case 1:
			return new Dirt(false, level);
		case 2:
			return new Dirt(true, level);
		case 3:
			return new Door(level);
		case 4:
			return new Wall(level);
		}

		return null;
	}

	/**
	 * Gets the from symbol.
	 *
	 * @param symbole the symbole
	 * @return the from symbol
	 */
	public static final int getFromSymbol(final char symbole)
	{
		switch (symbole)
		{
		case '+':
			return DIRT;
		case '-':
			return DIRTDIG;
		case '|':
			return DOOR;
		case 'x':
			return WALL;
		default:
			return DIRTDIG;
		}
	}
}