/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.elements.mobiles;

import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;

import contract.Direction;
import contract.IMap;
import contract.IPawn;
import contract.elements.ElementBehavior;
import contract.elements.IKilleable;
import contract.elements.IPlayer;
import contract.elements.ITriggering;
import model.actions.TriggerCollapseAction;
import model.animations.PlayerAnimation;
import model.elements.AnimationSpritesManager;

/**
 * The Class Player.
 */
public class Player extends MobileElement implements IPlayer, IKilleable, ITriggering
{

	/** The waiting animation sprites manager. */
	private AnimationSpritesManager waitingAnimationSpritesManager = new AnimationSpritesManager(this);

	/** The waiting images. */
	private final String[] waitingImages =
	{ "Player2.png", "Player3.png" };

	/** The right deplacement animation sprites manager. */
	private AnimationSpritesManager rightDeplacementAnimationSpritesManager = new AnimationSpritesManager(this);

	/** The right deplacement images. */
	private final String[] rightDeplacementImages =
	{ "PlayerRight1.png", "PlayerRight2.png", "PlayerRight3.png" };

	/** The left deplacement animation sprites manager. */
	private AnimationSpritesManager leftDeplacementAnimationSpritesManager = new AnimationSpritesManager(this);

	/** The left deplacement images. */
	private final String[] leftDeplacementImages =
	{ "PlayerLeft1.png", "PlayerLeft2.png", "PlayerLeft3.png" };

	/** The up deplacement animation sprites manager. */
	private AnimationSpritesManager upDeplacementAnimationSpritesManager = new AnimationSpritesManager(this);

	/** The up deplacement images. */
	private final String[] upDeplacementImages =
	{ "PlayerUp1.png", "PlayerUp2.png", "PlayerUp3.png", "PlayerUp4.png" };

	/** The down deplacement animation sprites manager. */
	private AnimationSpritesManager downDeplacementAnimationSpritesManager = new AnimationSpritesManager(this);

	/** The down deplacement images. */
	private final String[] downDeplacementImages =
	{ "PlayerDown1.png", "PlayerDown2.png", "PlayerDown3.png", "PlayerDown4.png" };

	/** The animation. */
	private PlayerAnimation animation;

	/** The alive. */
	private boolean alive = true;

	/** The death message. */
	private String deathMessage;

	/**
	 * Instantiates a new player.
	 *
	 * @param pos the pos
	 * @param map the map
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public Player(Point pos, IMap map) throws IOException
	{
		super(pos, map);
		this.setBehavior(ElementBehavior.PERMEABLE);
		this.initAnimationSpriteManager(this.waitingAnimationSpritesManager, this.waitingImages);
		this.initAnimationSpriteManager(this.upDeplacementAnimationSpritesManager, this.upDeplacementImages);
		this.initAnimationSpriteManager(this.rightDeplacementAnimationSpritesManager, this.rightDeplacementImages);
		this.initAnimationSpriteManager(this.downDeplacementAnimationSpritesManager, this.downDeplacementImages);
		this.initAnimationSpriteManager(this.leftDeplacementAnimationSpritesManager, this.leftDeplacementImages);
		this.setActiveSprite(this.waitingAnimationSpritesManager.get(0));
	}

	/* (non-Javadoc)
	 * @see contract.elements.IPlayer#startAnimation()
	 */
	@Override
	public void startAnimation()
	{
		this.animation = new PlayerAnimation(this, this.getMap().getGameModel());
	}

	/* (non-Javadoc)
	 * @see contract.elements.IPlayer#stopAnimation()
	 */
	@Override
	public void stopAnimation()
	{
		if (this.animation != null)
			this.animation.clear();

		this.animation = null;
	}

	/* (non-Javadoc)
	 * @see contract.elements.IPlayer#moveLeft()
	 */
	@Override
	public void moveLeft()
	{
		this.setPosition(new Point(this.getX() - 1, this.getY()));
		this.getAnimation().updateMovementAction(Direction.LEFT);

		this.startTriggerAround(ITriggering.getIPawnsToTrigger(this, this.getMap()), this.getMap());

		this.getMap().getGameModel().updateChanges();

	}

	/* (non-Javadoc)
	 * @see contract.elements.IPlayer#moveRight()
	 */
	@Override
	public void moveRight()
	{
		this.setPosition(new Point(this.getX() + 1, this.getY()));
		this.getAnimation().updateMovementAction(Direction.RIGHT);

		this.startTriggerAround(ITriggering.getIPawnsToTrigger(this, this.getMap()), this.getMap());

		this.getMap().getGameModel().updateChanges();
	}

	/* (non-Javadoc)
	 * @see contract.elements.IPlayer#moveUp()
	 */
	@Override
	public void moveUp()
	{
		this.setPosition(new Point(this.getX(), this.getY() - 1));
		this.getAnimation().updateMovementAction(Direction.UP);

		this.startTriggerAround(ITriggering.getIPawnsToTrigger(this, this.getMap()), this.getMap());

		this.getMap().getGameModel().updateChanges();
	}

	/* (non-Javadoc)
	 * @see contract.elements.IPlayer#moveDown()
	 */
	@Override
	public void moveDown()
	{
		ArrayList<IPawn> elemsToTrigger = ITriggering.getIPawnsToTrigger(this, this.getMap());

		this.setPosition(new Point(this.getX(), this.getY() + 1));
		this.getAnimation().updateMovementAction(Direction.DOWN);

		this.startTriggerAround(ITriggering.getIPawnsToTrigger(this, this.getMap()), this.getMap());

		this.getMap().getGameModel().updateChanges();
	}

	/* (non-Javadoc)
	 * @see contract.elements.ITriggering#startTriggerAround(java.util.ArrayList, contract.IMap)
	 */
	@Override
	public void startTriggerAround(ArrayList<IPawn> ipawnToTrigger, IMap map)
	{
		Thread triggerThread = new Thread(new TriggerCollapseAction(ipawnToTrigger, map));
		triggerThread.start();
	}

	/* (non-Javadoc)
	 * @see contract.elements.IPlayer#getPosX()
	 */
	@Override
	public int getPosX()
	{
		return this.getX();
	}

	/* (non-Javadoc)
	 * @see contract.elements.IPlayer#getPosY()
	 */
	@Override
	public int getPosY()
	{
		return this.getY();
	}

	/* (non-Javadoc)
	 * @see model.elements.Element#execCollisionAction()
	 */
	@Override
	public void execCollisionAction()
	{}

	/* (non-Javadoc)
	 * @see contract.elements.IKilleable#die(java.lang.String)
	 */
	@Override
	public void die(String deathMessage)
	{
		this.alive = false;
		this.deathMessage = deathMessage;
		this.getMap().getMobilesElements().remove(this);
	}

	/* (non-Javadoc)
	 * @see contract.elements.IPlayer#isAlive()
	 */
	@Override
	public boolean isAlive()
	{
		return this.alive;
	}

	/**
	 * Gets the waiting animation sprites manager.
	 *
	 * @return the waiting animation sprites manager
	 */
	public AnimationSpritesManager getWaitingAnimationSpritesManager()
	{
		return this.waitingAnimationSpritesManager;
	}

	/**
	 * Gets the up animation sprites manager.
	 *
	 * @return the up animation sprites manager
	 */
	public AnimationSpritesManager getUpAnimationSpritesManager()
	{
		return this.upDeplacementAnimationSpritesManager;
	}

	/**
	 * Gets the right animation sprites manager.
	 *
	 * @return the right animation sprites manager
	 */
	public AnimationSpritesManager getRightAnimationSpritesManager()
	{
		return this.rightDeplacementAnimationSpritesManager;
	}

	/**
	 * Gets the down animation sprites manager.
	 *
	 * @return the down animation sprites manager
	 */
	public AnimationSpritesManager getDownAnimationSpritesManager()
	{
		return this.downDeplacementAnimationSpritesManager;
	}

	/**
	 * Gets the left animation sprites manager.
	 *
	 * @return the left animation sprites manager
	 */
	public AnimationSpritesManager getLeftAnimationSpritesManager()
	{
		return this.leftDeplacementAnimationSpritesManager;
	}

	/**
	 * Gets the animation.
	 *
	 * @return the animation
	 */
	private PlayerAnimation getAnimation()
	{
		return this.animation;
	}

	/* (non-Javadoc)
	 * @see contract.elements.IPlayer#getDeathMessage()
	 */
	@Override
	public String getDeathMessage()
	{
		return this.deathMessage;
	}
}