/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.elements.mobiles;

import java.awt.Point;
import java.io.IOException;

import contract.IMap;
import contract.elements.ElementBehavior;
import contract.elements.IKilleable;
import contract.elements.IKiller;
import model.actions.MonsterMovementAction;
import model.elements.AnimationSpritesManager;
import model.sound.SoundManager;

/**
 * The Class Monster.
 */
public class Monster extends MobileElement implements IKilleable, IKiller
{

	/** The images sprites. */
	private final String[] imagesSprites =
	{ "Monster1.png", "Monster2.png", "Monster3.png", "Monster4.png" };

	/** The main animation sprites manager. */
	AnimationSpritesManager mainAnimationSpritesManager = new AnimationSpritesManager(this);

	/** The movement action. */
	private MonsterMovementAction movementAction = new MonsterMovementAction(this);

	/**
	 * Instantiates a new monster.
	 *
	 * @param pos the pos
	 * @param level the level
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public Monster(Point pos, IMap level) throws IOException
	{
		super(pos, level);
		this.setBehavior(ElementBehavior.PERMEABLE);
		this.initAnimationSpriteManager(this.mainAnimationSpritesManager, this.imagesSprites);
		this.setActiveSprite(this.getMainAnimationsSpritesManager().get(0));
	}

	/* (non-Javadoc)
	 * @see model.elements.Element#execCollisionAction()
	 */
	@Override
	public void execCollisionAction()
	{
		SoundManager.PlaySound(SoundManager.MONSTER_KILL_SOUND);
		((IKilleable) this.getMap().getGameModel().getPlayer()).die("killed by monster");
	}

	/**
	 * Start moving.
	 */
	public void startMoving()
	{
		this.getMovementAction().start();
	}

	/**
	 * Stop moving.
	 */
	public void stopMoving()
	{
		this.getMovementAction().stop();
	}

	/**
	 * Gets the main animations sprites manager.
	 *
	 * @return the main animations sprites manager
	 */
	public AnimationSpritesManager getMainAnimationsSpritesManager()
	{
		return this.mainAnimationSpritesManager;
	}

	/* (non-Javadoc)
	 * @see contract.elements.IKilleable#die(java.lang.String)
	 */
	@Override
	public void die(String deathMessage)
	{
		this.getMovementAction().stop();
		this.getMap().destroyIpawn(this);
	}

	/**
	 * Gets the movement action.
	 *
	 * @return the movement action
	 */
	private MonsterMovementAction getMovementAction()
	{
		return this.movementAction;
	}

	/* (non-Javadoc)
	 * @see contract.elements.IKiller#kill()
	 */
	@Override
	public void kill()
	{
		this.execCollisionAction();
	}

	/* (non-Javadoc)
	 * @see model.elements.mobiles.MobileElement#setPosition(java.awt.Point)
	 */
	@Override
	public void setPosition(Point pos)
	{
		super.setPosition(pos);

		if (!this.getMap().getGameModel().gameIsReloading())
			this.getMap().getGameModel().updateChanges();
	}
}