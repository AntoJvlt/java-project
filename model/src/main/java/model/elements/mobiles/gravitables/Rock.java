/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.elements.mobiles.gravitables;

import java.awt.Point;
import java.io.IOException;

import contract.IMap;
import contract.elements.ElementBehavior;
import contract.elements.IPushable;
import model.elements.AnimationSpritesManager;
import model.elements.Element;
import model.elements.motionless.Dirt;
import model.sound.SoundManager;

/**
 * The Class Rock.
 */
public class Rock extends GravitableElement implements IPushable
{

	/** The images sprites. */
	private final String[] imagesSprites =
	{ "Rock1.png", "Rock2.png", "Rock3.png", "Rock4.png" };

	/** The main animation sprites manager. */
	AnimationSpritesManager mainAnimationSpritesManager = new AnimationSpritesManager(this);

	/**
	 * Instantiates a new rock.
	 *
	 * @param pos the pos
	 * @param level the level
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public Rock(Point pos, IMap level) throws IOException
	{
		super(pos, level);
		this.setBehavior(ElementBehavior.BLOCKING);
		this.initAnimationSpriteManager(this.mainAnimationSpritesManager, this.imagesSprites);
		this.setActiveSprite(this.getMainAnimationSpritesManager().get(0));
	}

	/* (non-Javadoc)
	 * @see contract.elements.IPushable#pushRight(contract.IMap)
	 */
	@Override
	public void pushRight(IMap map)
	{
		SoundManager.PlaySound(SoundManager.ROCK_PUCH_SOUND);
		this.setPosition(new Point(this.getX() + 1, this.getY()));
		this.fall(map);
	}

	/* (non-Javadoc)
	 * @see contract.elements.IPushable#pushLeft(contract.IMap)
	 */
	@Override
	public void pushLeft(IMap map)
	{
		SoundManager.PlaySound(SoundManager.ROCK_PUCH_SOUND);
		this.setPosition(new Point(this.getX() - 1, this.getY()));
		this.fall(map);
	}

	/* (non-Javadoc)
	 * @see contract.elements.IPushable#canBePushedRight()
	 */
	@Override
	public boolean canBePushedRight()
	{
		if (!this.isFalling())
		{
			Element nextPosISquare = (Element) this.getMap().getElementOnTheMap(this.getX() + 1, this.getY());
			if (nextPosISquare instanceof Dirt && ((Dirt) nextPosISquare).isDug())
			{
				Element nextPosIPawn = (Element) this.getMap().searchIpawnAtPos(this.getX() + 1, this.getY());

				if (nextPosIPawn == null)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see contract.elements.IPushable#canBePushedLeft()
	 */
	@Override
	public boolean canBePushedLeft()
	{
		if (!this.isFalling())
		{
			Element nextPosISquare = (Element) this.getMap().getElementOnTheMap(this.getX() - 1, this.getY());
			if (nextPosISquare instanceof Dirt && ((Dirt) nextPosISquare).isDug())
			{
				Element nextPosIPawn = (Element) this.getMap().searchIpawnAtPos(this.getX() - 1, this.getY());

				if (nextPosIPawn == null)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see model.elements.Element#execCollisionAction()
	 */
	@Override
	public void execCollisionAction()
	{}

	/**
	 * Gets the main animation sprites manager.
	 *
	 * @return the main animation sprites manager
	 */
	public AnimationSpritesManager getMainAnimationSpritesManager()
	{
		return this.mainAnimationSpritesManager;
	}
}
