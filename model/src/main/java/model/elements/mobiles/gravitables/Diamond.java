/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.elements.mobiles.gravitables;

import java.awt.Point;
import java.io.IOException;

import contract.IMap;
import contract.elements.ElementBehavior;
import contract.elements.ICollectable;
import model.elements.AnimationSpritesManager;
import model.sound.SoundManager;

/**
 * The Class Diamond.
 */
public class Diamond extends GravitableElement implements ICollectable
{

	/** The images sprites. */
	private final String[] imagesSprites =
	{ "Diamond1.png", "Diamond2.png", "Diamond3.png", "Diamond4.png" };

	/** The main animation sprites manager. */
	AnimationSpritesManager mainAnimationSpritesManager = new AnimationSpritesManager(this);

	/** The nbr diamond total. */
	public static int nbrDiamondTotal = 0;

	/** The nbr diamond collected. */
	public static int nbrDiamondCollected = 0;

	/**
	 * Instantiates a new diamond.
	 *
	 * @param pos the pos
	 * @param level the level
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public Diamond(Point pos, IMap level) throws IOException
	{
		super(pos, level);
		this.setBehavior(ElementBehavior.BLOCKING);
		this.initAnimationSpriteManager(this.mainAnimationSpritesManager, this.imagesSprites);
		this.setActiveSprite(this.getMainAnimationSpritesManager().get(0));
		nbrDiamondTotal++;
	}

	/**
	 * Checks if is goal.
	 *
	 * @return true, if is goal
	 */
	public boolean isGoal()
	{
		if (nbrDiamondCollected >= nbrDiamondTotal / 2)
			return true;
		else
			return false;
	}

	/* (non-Javadoc)
	 * @see contract.elements.ICollectable#collect()
	 */
	@Override
	public void collect()
	{
		SoundManager.PlaySound(SoundManager.DIAMOND_PICK_SOUND);
		nbrDiamondCollected++;

		if (this.isGoal())
			this.getMap().getDoor().open();

		this.getMap().destroyIpawn(this);
	}

	/* (non-Javadoc)
	 * @see model.elements.Element#execCollisionAction()
	 */
	@Override
	public void execCollisionAction()
	{
		this.collect();
	}

	/**
	 * Gets the main animation sprites manager.
	 *
	 * @return the main animation sprites manager
	 */
	public AnimationSpritesManager getMainAnimationSpritesManager()
	{
		return this.mainAnimationSpritesManager;
	}
}
