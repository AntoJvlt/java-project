/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.elements.mobiles.gravitables;

import java.awt.Point;
import java.io.IOException;
import java.util.ArrayList;

import contract.IMap;
import contract.IPawn;
import contract.elements.IGravitable;
import model.actions.FallingAction;
import model.actions.TriggerCollapseAction;
import model.elements.mobiles.MobileElement;

/**
 * The Class GravitableElement.
 */
public abstract class GravitableElement extends MobileElement implements IGravitable
{

	/** The is falling. */
	private boolean isFalling = false;

	/**
	 * Instantiates a new gravitable element.
	 *
	 * @param pos the pos
	 * @param level the level
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public GravitableElement(Point pos, IMap level) throws IOException
	{
		super(pos, level);
	}

	/* (non-Javadoc)
	 * @see contract.elements.IGravitable#fall(contract.IMap)
	 */
	@Override
	public void fall(IMap map)
	{
		Thread fallingActionThread = new Thread(new FallingAction(this, map));
		fallingActionThread.start();
	}

	/* (non-Javadoc)
	 * @see contract.elements.IGravitable#isFalling()
	 */
	@Override
	public boolean isFalling()
	{
		return this.isFalling;
	}

	/**
	 * Sets the falling.
	 *
	 * @param isFalling the new falling
	 */
	public void setFalling(boolean isFalling)
	{
		this.isFalling = isFalling;
	}

	/* (non-Javadoc)
	 * @see contract.elements.ITriggering#startTriggerAround(java.util.ArrayList, contract.IMap)
	 */
	@Override
	public void startTriggerAround(ArrayList<IPawn> elemsToTrigger, IMap map)
	{
		Thread triggerThread = new Thread(new TriggerCollapseAction(elemsToTrigger, map));
		triggerThread.start();
	}

}
