/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.elements.mobiles;

import java.awt.Point;
import java.io.IOException;

import contract.IMap;
import contract.IPawn;
import model.elements.AnimationSpritesManager;
import model.elements.Element;
import model.elements.Sprite;

/**
 * The Class MobileElement.
 */
public abstract class MobileElement extends Element implements IPawn
{

	/** The pos. */
	private Point pos;

	/** The animation sprites manager. */
	AnimationSpritesManager animationSpritesManager;

	/**
	 * Instantiates a new mobile element.
	 *
	 * @param pos the pos
	 * @param level the level
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public MobileElement(Point pos, IMap level) throws IOException
	{
		super(level);
		this.setPosition(pos);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.exia.showboard.IPawn#getPosition()
	 */
	@Override
	public Point getPosition()
	{
		return this.pos;
	}

	/**
	 * Sets the position.
	 *
	 * @param pos the new position
	 */
	public void setPosition(Point pos)
	{
		this.pos = pos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.exia.showboard.IPawn#getX()
	 */
	@Override
	public int getX()
	{
		return this.getPosition().x;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.exia.showboard.IPawn#getY()
	 */
	@Override
	public int getY()
	{
		return this.getPosition().y;
	}

	/**
	 * Inits the animation sprite manager.
	 *
	 * @param animationSpritesManager the animation sprites manager
	 * @param spritesImages the sprites images
	 */
	protected void initAnimationSpriteManager(AnimationSpritesManager animationSpritesManager, String[] spritesImages)
	{
		for (String imageSprite : spritesImages)
		{
			try
			{
				animationSpritesManager.add(new Sprite(imageSprite));
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}
}
