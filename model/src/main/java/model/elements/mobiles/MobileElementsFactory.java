/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */

package model.elements.mobiles;

import java.awt.Point;
import java.io.IOException;

import contract.IMap;
import model.elements.mobiles.gravitables.Diamond;
import model.elements.mobiles.gravitables.Rock;

// TODO: Auto-generated Javadoc
/**
 * A factory for creating MobileElements objects.
 */
public final class MobileElementsFactory
{

	/** The Constant DIAMOND. */
	public final static int DIAMOND = 1;

	/** The Constant ROCK. */
	public final static int ROCK = 2;

	/** The Constant MONSTER. */
	public final static int MONSTER = 3;

	/** The Constant PLAYER. */
	public final static int PLAYER = 4;

	/**
	 * Creates a new MobileElements object.
	 *
	 * @param mobileElement the mobile element
	 * @param pos the pos
	 * @param level the level
	 * @return the mobile element
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static final MobileElement createMobileElement(final int mobileElement, Point pos, IMap level) throws IOException
	{
		switch (mobileElement)
		{
		case 1:
			return new Diamond(pos, level);
		case 2:
			return new Rock(pos, level);
		case 3:
			return new Monster(pos, level);
		case 4:
			return new Player(pos, level);
		}

		return null;
	}

	/**
	 * Gets the from symbol.
	 *
	 * @param symbole the symbole
	 * @return the from symbol
	 */
	public static final int getFromSymbol(final char symbole)
	{
		switch (symbole)
		{
		case 'D':
			return DIAMOND;
		case 'o':
			return ROCK;
		case 'P':
			return PLAYER;
		case 'M':
			return MONSTER;
		default:
			return 0;
		}
	}
}