/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.elements;

import java.util.ArrayList;

/**
 * The Class AnimationSpritesManager.
 * 
 * Handle the sprites of an element for the animation.
 */
public final class AnimationSpritesManager extends ArrayList<Sprite>
{

	/** The current index. */
	private int currentIndex = 0;

	/** The element. */
	private Element element;

	/**
	 * Instantiates a new animation sprites manager.
	 *
	 * @param elem the elem
	 */
	public AnimationSpritesManager(Element elem)
	{
		this.setElement(elem);
	}

	/**
	 * Next animation step.
	 */
	public void nextAnimationStep()
	{
		this.setCurrentIndex((this.getCurrentIndex() + 1) % this.size());
	}

	/**
	 * Gets the current index.
	 *
	 * @return the current index
	 */
	public int getCurrentIndex()
	{
		return this.currentIndex;
	}

	/**
	 * Sets the current index.
	 *
	 * @param currentIndex the new current index
	 */
	public void setCurrentIndex(int currentIndex)
	{
		this.currentIndex = currentIndex;
	}

	/**
	 * Gets the element.
	 *
	 * @return the element
	 */
	public Element getElement()
	{
		return this.element;
	}

	/**
	 * Sets the element.
	 *
	 * @param element the new element
	 */
	public void setElement(Element element)
	{
		this.element = element;
	}
}
