/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model;

import java.awt.Point;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

import contract.IGameModel;
import contract.IMap;
import contract.IPawn;
import contract.ISquare;
import contract.elements.IDoor;
import entity.MapPattern;
import model.database.DAOMap;
import model.database.DBConnection;
import model.elements.Element;
import model.elements.mobiles.MobileElementsFactory;
import model.elements.motionless.Door;
import model.elements.motionless.MotionlessElementsFactory;

/**
 * The Class Map.
 * 
 * The map of the game, loaded from the map pattern which was extracted from the data base.
 */
public class Map implements IMap
{
	/** The map pattern. */
	private MapPattern mapPattern;

	/** The map. */
	private String map;

	/** The is being modified. */
	private boolean isBeingModified = false;

	/** The mobiles elements. */
	private ArrayList<IPawn> mobilesElements = new ArrayList<>();

	/** The map width. */
	private int mapWidth;

	/** The map height. */
	private int mapHeight;

	/** The tab map. */

	/** Map in 2 Dimension tab */
	private ISquare tabMap[][];

	/** The game model. */
	private IGameModel gameModel;

	/** The door. */
	private IDoor door;

	/**
	 * Instantiates a new map.
	 *
	 * @param LEVEL_NUMBER the level number
	 * @param gameModel the game model
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws SQLException the SQL exception
	 */
	public Map(int LEVEL_NUMBER, IGameModel gameModel) throws IOException, SQLException
	{
		DAOMap daoLevel = new DAOMap(DBConnection.getInstance().getConnection());
		this.setMapPattern(daoLevel.find(LEVEL_NUMBER));
		this.setMap(mapPattern.getMap());
		this.gameModel = gameModel;
		this.initDimensions();
		this.tabMap = new Element[this.mapWidth][this.mapHeight];
		this.fillMap();
		while (!this.initDoor());
	}

	/* (non-Javadoc)
	 * @see contract.IMap#getTabMap()
	 */
	@Override
	public ISquare[][] getTabMap()
	{
		return this.tabMap;
	}

	/**
	 * Initialize the dimensions of the map.
	 */
	public void initDimensions()
	{

		int columnMap = 0;
		int widthMap = 0;
		boolean widthCounted = false;

		/*
		 * Get the number of characters in map
		 */
		int lenghtStringMap = map.length();

		for (int i = 0; i < lenghtStringMap; i++)
		{
			/*
			 * The charAt method finds the position and convert to char
			 */
			char charVar = map.charAt(i);
			/*
			 * If there is a line break, we go to the next line
			 */
			if (charVar == '/')
			{
				columnMap++;

				if (!widthCounted)
				{
					this.mapWidth = widthMap;
					widthCounted = true;
				}
			}
			/*
			 * If there is no return to the line we write
			 */
			else
			{
				widthMap++;
			}
		}
		this.mapHeight = columnMap;
	}

	/**
	 * Fill the map with the elements.
	 * Convert the string map pattern into elements.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void fillMap() throws IOException
	{
		int heightMap = 0;
		int widthMap = 0;
		/*
		 * Get the number of characters in map
		 */
		int lenghtStringMap = map.length();

		for (int i = 0; i < lenghtStringMap; i++)
		{
			/*
			 * The charAt method finds the position and convert to char
			 */
			char charVar = map.charAt(i);
			/*
			 * If there is a line break, we go to the next line
			 */
			if (charVar == '/')
			{
				heightMap++;
				widthMap = 0;
			}
			/*
			 * If there is no return to the line we write
			 */
			else
			{
				tabMap[widthMap][heightMap] = MotionlessElementsFactory.createMotionlessElement(MotionlessElementsFactory.getFromSymbol(charVar), this);

				if (MobileElementsFactory.getFromSymbol(charVar) != 0)
				{
					this.addMobilesElements(MobileElementsFactory.createMobileElement(MobileElementsFactory.getFromSymbol(charVar), new Point(widthMap, heightMap), this));
				}

				widthMap++;
			}
		}
	}

	/**
	 * Reset the map.
	 *
	 * @return true, if successful
	 */
	public boolean reset()
	{
		this.isBeingModified = true;

		this.clearPawns();

		for (int x = 0; x < this.getMapWidth(); x++)
			for (int y = 0; y < this.getMapHeight(); y++)
				try
				{
					this.getTabMap()[x][y] = MotionlessElementsFactory.createMotionlessElement(MotionlessElementsFactory.WALL, this);
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}

		this.isBeingModified = false;
		this.getGameModel().updateChanges();
		this.isBeingModified = true;

		try
		{
			this.fillMap();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		while (!this.initDoor());

		this.isBeingModified = false;

		return true;
	}

	/**
	 * Clear the IPawns elements of the map.
	 */
	private void clearPawns()
	{
		Iterator<IPawn> iterator = this.getMobilesElements().iterator();

		while (iterator.hasNext())
		{
			IPawn pawnNext = iterator.next();

			iterator.remove();
		}
	}

	/**
	 * Inits the door.
	 *
	 * @return true, if successful
	 */
	public boolean initDoor()
	{
		for (int x = 0; x < this.getMapWidth(); x++)
			for (int y = 0; y < this.getMapHeight(); y++)
				if (this.getElementOnTheMap(x, y) instanceof Door)
					this.door = (Door) this.getElementOnTheMap(x, y);

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see contract.ILevel#getMobilesElements()
	 */
	@Override
	public ArrayList<IPawn> getMobilesElements()
	{
		return this.mobilesElements;
	}

	/**
	 * Adds the mobiles elements.
	 *
	 * @param mobileElement the mobile element
	 */
	public void addMobilesElements(IPawn mobileElement)
	{
		this.getMobilesElements().add(mobileElement);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see contract.ILevel#getMapWidth()
	 */
	@Override
	public int getMapWidth()
	{
		return this.mapWidth;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see contract.ILevel#getMapHeight()
	 */
	@Override
	public int getMapHeight()
	{
		return this.mapHeight;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see contract.ILevel#getElementOnTheMap(int, int)
	 */
	@Override
	public ISquare getElementOnTheMap(int x, int y)
	{
		return this.tabMap[x][y];
	}

	/* (non-Javadoc)
	 * @see contract.IMap#searchIpawnAtPos(int, int)
	 */
	@Override
	public IPawn searchIpawnAtPos(int x, int y)
	{
		if (!this.isBeingModified())
		{

			Iterator<IPawn> iterator = this.getMobilesElements().iterator();

			while (iterator.hasNext())
			{
				IPawn pawnNext = iterator.next();

				if (pawnNext.getX() == x && pawnNext.getY() == y)
					return pawnNext;

			}
		}

		return null;
	}

	/* (non-Javadoc)
	 * @see contract.IMap#destroyIpawn(contract.IPawn)
	 */
	@Override
	public void destroyIpawn(IPawn ipawn)
	{
		this.isBeingModified = true;

		Iterator<IPawn> iterator = this.getMobilesElements().iterator();

		while (iterator.hasNext())
		{
			IPawn pawnNext = iterator.next();

			if (ipawn == pawnNext)
				iterator.remove();
		}

		this.isBeingModified = false;
	}

	/* (non-Javadoc)
	 * @see contract.IMap#getGameModel()
	 */
	@Override
	public IGameModel getGameModel()
	{
		return this.gameModel;
	}

	/**
	 * Gets the map pattern.
	 *
	 * @return the map pattern
	 */
	public MapPattern getMapPattern()
	{
		return mapPattern;
	}

	/**
	 * Sets the map pattern.
	 *
	 * @param mapPattern the new map pattern
	 */
	public void setMapPattern(MapPattern mapPattern)
	{
		this.mapPattern = mapPattern;
	}

	/* (non-Javadoc)
	 * @see contract.IMap#getDoor()
	 */
	@Override
	public IDoor getDoor()
	{
		return this.door;
	}

	/* (non-Javadoc)
	 * @see contract.IMap#isBeingModified()
	 */
	@Override
	public boolean isBeingModified()
	{
		return this.isBeingModified;
	}

	/**
	 * Gets the map.
	 *
	 * @return the map
	 */
	public String getMap()
	{
		return map;
	}

	/**
	 * Sets the map.
	 *
	 * @param map the new map
	 */
	public void setMap(String map)
	{
		this.map = map;
	}
}