/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.sound;

/**
 * The Class RockKillSound.
 */
public class RockKillSound extends Sound
{

	/** The file name. */
	private static String FILE_NAME = "Sound/die.wav";

	/** The volume. */
	private static float VOLUME = 6;

	/**
	 * Instantiates a new rock kill sound.
	 */
	public RockKillSound()
	{
		super(FILE_NAME, VOLUME);
	}
}
