/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.sound;

/**
 * The Class MonsterKillSound.
 */
public class MonsterKillSound extends Sound
{

	/** The file name. */
	private static String FILE_NAME = "Sound/killsound.wav";

	/** The volume. */
	private static float VOLUME = 6;

	/**
	 * Instantiates a new monster kill sound.
	 */
	public MonsterKillSound()
	{
		super(FILE_NAME, VOLUME);
	}
}
