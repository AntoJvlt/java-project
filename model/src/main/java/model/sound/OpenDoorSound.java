/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.sound;

/**
 * The Class OpenDoorSound.
 */
public class OpenDoorSound extends Sound
{

	/** The file name. */
	private static String FILE_NAME = "Sound/cash.wav";

	/** The volume. */
	private static float VOLUME = 6;

	/**
	 * Instantiates a new open door sound.
	 */
	public OpenDoorSound()
	{
		super(FILE_NAME, VOLUME);
	}
}
