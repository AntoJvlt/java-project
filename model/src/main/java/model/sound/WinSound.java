/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.sound;

/**
 * The Class WinSound.
 */
public class WinSound extends Sound
{

	/** The file name. */
	private static String FILE_NAME = "Sound/win.wav";

	/** The volume. */
	private static float VOLUME = 6;

	/**
	 * Instantiates a new win sound.
	 */
	public WinSound()
	{
		super(FILE_NAME, VOLUME);
	}
}
