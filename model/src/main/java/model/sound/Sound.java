/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.sound;

import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * The Class Sound.
 */
public abstract class Sound
{

	/** The clip. */
	private Clip clip;

	/** The audio in. */
	private AudioInputStream audioIn;

	/** The file name. */
	private String fileName = "";

	/**
	 * Instantiates a new sound.
	 *
	 * @param fileName2 the file name 2
	 * @param volume the volume
	 */
	public Sound(String fileName2, float volume)
	{
		this.fileName = fileName2;
		this.SetMusic();
		this.setVolume(volume);
		this.StartMusic();

	}

	/**
	 * Sets the music.
	 */
	// Open audio clip and load samples from the audio input stream.
	public void SetMusic()
	{
		InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(this.fileName);
		try
		{
			this.audioIn = AudioSystem.getAudioInputStream(inputStream);
		}
		catch (UnsupportedAudioFileException | IOException e)
		{
			e.printStackTrace();
		}
		// Get a sound clip resource.
		try
		{
			this.clip = AudioSystem.getClip();
			this.clip.open(audioIn);
		}
		catch (LineUnavailableException | IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Sets the volume.
	 *
	 * @param level the new volume
	 */
	public void setVolume(float level)
	{
		FloatControl gainControl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
		gainControl.setValue(level); // Reduce volume by 10 decibels.
	}

	/**
	 * Start music.
	 */
	public void StartMusic()
	{
		clip.start();
	}
}
