/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.sound;

/**
 * The Class SoundManager.
 */
public final class SoundManager
{

	/** The Constant DIAMOND_PICK_SOUND. */
	public final static int DIAMOND_PICK_SOUND = 1;

	/** The Constant ROCK_PUCH_SOUND. */
	public final static int ROCK_PUCH_SOUND = 2;

	/** The Constant DIRT_DIG_SOUND. */
	public final static int DIRT_DIG_SOUND = 3;

	/** The Constant START_SOUND. */
	public final static int START_SOUND = 4;

	/** The Constant TIMER_SOUND. */
	public final static int TIMER_SOUND = 5;

	/** The Constant OPEN_DOOR_SOUND. */
	public final static int OPEN_DOOR_SOUND = 6;

	/** The Constant WIN_SOUND. */
	public final static int WIN_SOUND = 7;

	/** The Constant MONSTER_KILL_SOUND. */
	public final static int MONSTER_KILL_SOUND = 8;

	/** The Constant ROCK_KILL_SOUND. */
	public final static int ROCK_KILL_SOUND = 9;

	/**
	 * Play sound.
	 *
	 * @param sound the sound
	 * @return the sound
	 */
	public static final Sound PlaySound(int sound)
	{
		switch (sound)
		{
		case 1:
			return new DiamondPickSound();
		case 2:
			return new RockPushSound();
		case 3:
			return new DirtSound();
		case 4:
			return new StartSound();
		case 5:
			return new TimerSound();
		case 6:
			return new OpenDoorSound();
		case 7:
			return new WinSound();
		case 8:
			return new MonsterKillSound();
		case 9:
			return new RockKillSound();
		}
		return null;
	}

}
