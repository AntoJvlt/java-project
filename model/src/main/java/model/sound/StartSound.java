/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.sound;

/**
 * The Class StartSound.
 */
public class StartSound extends Sound
{

	/** The file name. */
	private static String FILE_NAME = "Sound/StartMusic.wav";

	/** The volume. */
	private static float VOLUME = -10;

	/**
	 * Instantiates a new start sound.
	 */
	public StartSound()
	{
		super(FILE_NAME, VOLUME);
	}
}
