/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.sound;

/**
 * The Class TimerSound.
 */
public class TimerSound extends Sound
{

	/** The file name. */
	private static String FILE_NAME = "Sound/chrono.wav";

	/** The volume. */
	private static float VOLUME = 6;

	/**
	 * Instantiates a new timer sound.
	 */
	public TimerSound()
	{
		super(FILE_NAME, VOLUME);
	}
}
