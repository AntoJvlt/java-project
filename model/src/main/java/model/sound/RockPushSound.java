/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.sound;

/**
 * The Class RockPushSound.
 */
public class RockPushSound extends Sound
{

	/** The file name. */
	private static String FILE_NAME = "Sound/Rock.wav";

	/** The volume. */
	private static float VOLUME = 0;

	/**
	 * Instantiates a new rock push sound.
	 */
	public RockPushSound()
	{
		super(FILE_NAME, VOLUME);
	}
}
