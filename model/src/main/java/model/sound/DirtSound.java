/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.sound;

/**
 * The Class DirtSound.
 */
public class DirtSound extends Sound
{

	/** The file name. */
	private static String FILE_NAME = "Sound/Dirt.wav";

	/** The volume. */
	private static float VOLUME = 0;

	/**
	 * Instantiates a new dirt sound.
	 */
	public DirtSound()
	{
		super(FILE_NAME, VOLUME);
	}
}
