/*
 * @author Jolivat Antonin
 * @author Doittée Anthime
 * @author Zaafane Rania
 * @author Favier Paulin
 */
package model.sound;

/**
 * The Class DiamondPickSound.
 */
public class DiamondPickSound extends Sound
{

	/** The file name. */
	private static String FILE_NAME = "Sound/Diamond.wav";

	/** The volume. */
	private static float VOLUME = 0;

	/**
	 * Instantiates a new diamond pick sound.
	 */
	public DiamondPickSound()
	{
		super(FILE_NAME, VOLUME);
	}
}
