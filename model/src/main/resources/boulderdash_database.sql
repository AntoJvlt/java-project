-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 04 juin 2019 à 15:54
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `java_project`
--
CREATE DATABASE IF NOT EXISTS `java_project` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `java_project`;

DELIMITER $$
--
-- Procédures
--
DROP PROCEDURE IF EXISTS `Map_boulderdashByLevel`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Map_boulderdashByLevel` (IN `p_id` INT(1))  NO SQL
SELECT Map FROM java_project.map_boulderdash where `id`=p_id$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Structure de la table `map_boulderdash`
--

DROP TABLE IF EXISTS `map_boulderdash`;
CREATE TABLE IF NOT EXISTS `map_boulderdash` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Map` varchar(65500) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=MyISAM AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `map_boulderdash`
--

INSERT INTO `map_boulderdash` (`ID`, `Map`) VALUES
(1, '+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++xxxxxxxxxxxxxxxxxxxxxxxx++++++++++/+++++++++++xD------M-D+++o+o+++++|x++++++++++/+++++++++++x--------o+++o++++oo+oDx++++++++++/+++++++++++x+++++++-+++++-++++++++x++++++++++/+++++++++++xP+++++ooo+o+oooo++oooox++++++++++/+++++++++++x+++++++++oo+o+-+o+-+o+x++++++++++/+++++++++++x+++++D+++++++++++++++dx++++++++++/+++++++++++x+++++++++oooooooo+++++x++++++++++/+++++++++++x++++++++++ooooo+++++++x++++++++++/+++++++++++x++oooo++++++++++++++++x++++++++++/+++++++++++x--o-D--++++++++++oooo+x++++++++++/+++++++++++x+++++++++++++++++DD+++x++++++++++/+++++++++++xxxxxxxxxxxxxxxxxxxxxxxx++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/'),
(2, 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx/x++Do+o+++++D++++++++++++++D+++++++++++++++++++++++D++++++++++++x/x++++oD+++++++++ooo++++++++++o+o++++D++++o+++++++++++o++++++++++x/x++++++++++++++++++++ooooooooooooooooo--------ooooooo+++++++++++x/xP+++++++++++++++++++--o+-++++++++++D++++++++++++ooooo++++-----|x/x+o+o+o+o----ooo++++++++o+++++++++++++++ooo++++o+++++++++++++-++x/x+o+o+o+o----DDo++++++++o+++++++++++++++ooo++o+o+++++++++++++-++x/x+o+o+o+o----ooo++++++++o+++++++++++++++ooo++++o+++++++++++++-++x/x+o+o+o+o----ooo++++++++o+++++++++++++++ooo++++o+++++++++++++-++x/x+o+o+o+o----ooo++++++++o+++++++++++++++ooo++++o+++++++++++++-++x/x+o+o+o+o----ooo++++++++o+++++++++++++++ooo++++o+++++++++++++-++x/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx/'),
(3, '+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++xxxxxxxxxxxxxxxxxxxxxxxx++++++++++/+++++++++++xo++++++++++++o+o++++++x++++++++++/+++++++++++xo+o+o---o+++o+++---+oDx++++++++++/+++++++++++x+++++++-+DDD+-++-+--++x++++++++++/+++++++++++xP+++++o-oDoDo+D+M+--oox++++++++++/+++++++++++x++++++oo-oooo+ooo+-+o+x++++++++++/+++++++++++x+++++Doooooo+ooo+++++dx++++++++++/+++++++++++xoo+oo+ooooo+ooooo+++++x++++++++++/+++++++++++x++++++++++++++++++++++x++++++++++/+++++++++++x----------------------x++++++++++/+++++++++++x-----------------D----x++++++++++/+++++++++++x---------------------|x++++++++++/+++++++++++xxxxxxxxxxxxxxxxxxxxxxxx++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/+++++++++++++++++++++++++++++++++++++++++++++/'),
(4, 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx/x++++++++++++o++o++++++oD+++++++++++++++o+o++o+o+ox/xD+++P++o----oD++++++++++++++++D+++++++oooo+++++++x/xxxxxxxxxxxxxxxxxxxxxx++++++++++++++xxxxxxxxxxxxx+x/xM-----++++++++++++++++++oooDDD++++++++M---+++D+D+x/xD++++++o----oD++++++++++++++++D+++++++oooo+++|+++x/xxxxxxxxxxxxxxxxxxxxxx++++++++++++++xxxxxxxxxxxxx+x/x++++++oooo++++++++DD++++++++++++++++o++o+o+o+o+++x/x+++++-------++++++++++++oooDDD+++++++++++++++D+D+x/xD++++M------oD++++++++++++++++D+++++++oooo+++++++x/xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx/'),
(5, 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxx/xP-----------------D--------x/xxxxxo----------ooooo----ooox/xM---+++++++++++++++++++++++x/xD-+++++++++++++++++++++++++x/xxxxx++++++++++++++++-----D+x/xD----xxxxxxxxxxxxxxxxxxxxx+x/x++++++++++++++++++++++++++ox/xxxxx-------------D--------+x/xMDDD+++++++D+++++++++++---+|/xxxxxxxxxxxxxxxxxxxxxxxxxxxxx/'),
(99, 'x-DPo|/xxxx--/');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
