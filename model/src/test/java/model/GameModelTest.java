/*
 * @author Jolivat Antonin - Favier Paulin - Doittée Anthime - Zaafane Rania
 */
package model;

import static org.junit.Assert.*;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.Test;

import model.elements.mobiles.gravitables.Diamond;

/**
 * The Class GameModelTest.
 */
public class GameModelTest
{

	/** The numero map test. */
	final int NUMERO_MAP_TEST = 99;

	/**
	 * Test reset game.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws SQLException the SQL exception
	 */
	@Test
	public void testResetGame() throws IOException, SQLException
	{
		GameModel gamemodel = new GameModel();
		Map map = new Map(NUMERO_MAP_TEST, gamemodel);
		Diamond.nbrDiamondCollected = 2;
		map.getGameModel().resetGame();
		assertEquals(gamemodel.getNumberDiamondCollected(), 0);

	}

}
