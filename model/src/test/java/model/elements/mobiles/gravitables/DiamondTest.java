/*
 * @author Jolivat Antonin - Favier Paulin - Doittée Anthime - Zaafane Rania
 */
package model.elements.mobiles.gravitables;

import static org.junit.Assert.*;

import java.awt.Point;
import java.io.IOException;
import java.sql.SQLException;

import org.junit.Test;

import model.GameModel;
import model.Map;

/**
 * The Class DiamondTest.
 */
public class DiamondTest
{

	/**
	 * Test collect can win and test isGoal.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws SQLException the SQL exception
	 */
	@Test
	public void testCollectCanWin() throws IOException, SQLException
	{
		GameModel gamemodel = new GameModel();
		Map map = new Map(99, gamemodel);
		Point pos = new Point(12, 12);
		Diamond diamond = new Diamond(pos, map);
		diamond.nbrDiamondCollected = 2;
		diamond.nbrDiamondTotal = 3;
		diamond.collect();
		assertTrue(diamond.getMap().getDoor().isOpen());
	}

	/**
	 * Test collect can't win and test isGoal.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws SQLException the SQL exception
	 */
	@Test
	public void testCollectCantWin() throws IOException, SQLException
	{
		GameModel gamemodel = new GameModel();
		Map map = new Map(99, gamemodel);
		Point pos = new Point(12, 12);
		Diamond diamond = new Diamond(pos, map);
		diamond.nbrDiamondCollected = 2;
		diamond.nbrDiamondTotal = 7;
		diamond.collect();
		assertTrue(diamond.getMap().getDoor().isOpen());
	}

}
