/*
 * @author Jolivat Antonin - Favier Paulin - Doittée Anthime - Zaafane Rania
 */
package model.elements.mobiles.gravitables;

import static org.junit.Assert.*;

import java.awt.Point;
import java.io.IOException;
import java.sql.SQLException;

import org.junit.Test;

import model.GameModel;
import model.Map;

/**
 * The Class RockTest.
 */
public class RockTest
{

	/**
	 * Test can be pushed right.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws SQLException the SQL exception
	 */
	@Test
	public void testCanBePushedRight() throws IOException, SQLException
	{
		GameModel gamemodel = new GameModel();
		Map map = new Map(99, gamemodel);
		Rock rock = new Rock(new Point(4, 1), map);
		assertTrue(rock.canBePushedRight());
	}

	/**
	 * Test cant be pushed left.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws SQLException the SQL exception
	 */
	@Test
	public void testCantBePushedLeft() throws IOException, SQLException
	{
		GameModel gamemodel = new GameModel();
		Map map = new Map(99, gamemodel);
		Rock rock = new Rock(new Point(4, 1), map);
		assertFalse(rock.canBePushedLeft());
	}

}
