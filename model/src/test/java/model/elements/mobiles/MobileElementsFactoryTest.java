/*
 * @author Jolivat Antonin - Favier Paulin - Doittée Anthime - Zaafane Rania
 */
package model.elements.mobiles;

import static org.junit.Assert.*;

import java.awt.Point;
import java.io.IOException;
import java.sql.SQLException;

import org.junit.Test;

import model.GameModel;
import model.Map;

/**
 * The Class MobileElementsFactoryTest.
 */
public class MobileElementsFactoryTest
{

	/**
	 * Test create mobile element.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws SQLException the SQL exception
	 */
	@Test
	public void testCreateMobileElement() throws IOException, SQLException
	{
		GameModel gamemodel = new GameModel();
		Map map = new Map(99, gamemodel);
		Point pos = new Point(3, 4);
		assertEquals(MobileElementsFactory.createMobileElement(MobileElementsFactory.ROCK, pos, map).getClass().getTypeName(), "model.elements.mobiles.gravitables.Rock");
	}

}
