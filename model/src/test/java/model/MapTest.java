/*
 * @author Jolivat Antonin - Favier Paulin - Doittée Anthime - Zaafane Rania
 */
package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import java.io.IOException;
import java.sql.SQLException;
import org.junit.Test;

/**
 * The Class MapTest.
 */
public class MapTest
{

	/** The numero map test. */
	final int NUMERO_MAP_TEST = 99;

	/**
	 * Test map height.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws SQLException the SQL exception
	 */
	@Test
	public void testMapHeight() throws IOException, SQLException
	{

		GameModel gamemodel = new GameModel();
		Map map = new Map(NUMERO_MAP_TEST, gamemodel);
		assertEquals(map.getMapHeight(), 2);
	}

	/**
	 * Test map width.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws SQLException the SQL exception
	 */
	@Test
	public void testMapWidth() throws IOException, SQLException
	{

		GameModel gamemodel = new GameModel();
		Map map = new Map(NUMERO_MAP_TEST, gamemodel);
		assertEquals(map.getMapWidth(), 6);
	}

	/**
	 * Test get element on the map.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws SQLException the SQL exception
	 */
	@Test
	public void testGetElementOnTheMap() throws IOException, SQLException
	{

		GameModel gamemodel = new GameModel();
		Map map = new Map(NUMERO_MAP_TEST, gamemodel);
		assertEquals(map.getElementOnTheMap(0, 0).getClass().getTypeName(), "model.elements.motionless.Wall");
	}

	/**
	 * Test search ipawn at pos.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws SQLException the SQL exception
	 */
	@Test
	public void testSearchIpawnAtPos() throws IOException, SQLException
	{

		GameModel gamemodel = new GameModel();
		Map map = new Map(NUMERO_MAP_TEST, gamemodel);
		assertEquals(map.searchIpawnAtPos(4, 0).getClass().getTypeName(), "model.elements.mobiles.gravitables.Rock");
	}

	/**
	 * Test destroy ipawn.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws SQLException the SQL exception
	 */
	@Test
	public void testDestroyIpawn() throws IOException, SQLException
	{
		GameModel gamemodel = new GameModel();
		Map map = new Map(NUMERO_MAP_TEST, gamemodel);
		map.destroyIpawn(map.searchIpawnAtPos(4, 0));
		assertNull(map.searchIpawnAtPos(4, 0));

	}
}
