/*
 * @author Jolivat Antonin - Favier Paulin - Doittée Anthime - Zaafane Rania
 */
package model.sound;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * The Class SoundManagerTest.
 */
public class SoundManagerTest
{

	/**
	 * Test play sound.
	 */
	@Test
	public void testPlaySound()
	{
		assertEquals(SoundManager.PlaySound(SoundManager.DIAMOND_PICK_SOUND).getClass().getTypeName(), "model.sound.DiamondPickSound");
	}

}
